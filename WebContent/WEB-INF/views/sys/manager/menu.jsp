<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- CSS -->
<link id="essyuicss" rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/metro-blue/easyui.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/common/common.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/theme-1/theme.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/icon.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/color.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/demo/demo.css" type="text/css" />
<title>Insert title here</title>
</head>
<body class="easyui-layout">
	
	<input id="menu_id" type="hidden">
    <div data-options="region:'west',title:'菜单',collapsible:false" style="width:300px;">
    	<div id="menuTree"></div>
    </div>   
    <div data-options="region:'center',title:'详细信息',lines:true" style="padding:5px;background:#eee;">
    	<div id="menuBox" class="easyui-layout" style="width:100%;height:100%;">   
		    <div data-options="region:'north',split:true" style="height:38%;">
		    	<form id="MenuForm" method="post"> 
		    		<input type="hidden" name="id" id="menuId"/> 
		    		<input type="hidden" name="pid" id="menuPid"/> 
		    		<input type="hidden" name="isLeaf" id="menuIsLeaf"/> 
		    		<input type="hidden" name="level" id="menuLevel"/>
		    		<div style="margin:15px">
			    		<label>菜单名称</label>
			    		<input class="easyui-validatebox" id="name" name="name" data-options="required:true,disabled:false"/>
		    		</div> 
		    		<div style="margin:15px">
			    		<label>映射路径</label>
			    		<input class="easyui-validatebox" id="url" name="url"/><br/>
		    		</div> 
		    		<div style="margin:15px">
			    		<label>排序位置</label>
			    		<input class="easyui-numberspinner" id="orderNum" name="orderNum" data-options="increment:1" style="width:120px;"></input>
		    		</div> 
		    		<div style="margin:15px">
			    		<label>菜单说明</label>
			    		<input class="easyui-validatebox" id="info" name="info" data-options="validType:'length[0,300]'" style="height: 60px; width: 338px;"/><br/>
					</div>
					<div style="margin-top:10px;margin-left:3px;text-align:center">
				    	<a id="editMenu" href="#" class="easyui-linkbutton" data-options="disabled:true" style="width:85px; height:23px;margin-left:10px">编辑</a>
				    	<a id="deleteMenu" href="#" class="easyui-linkbutton" data-options="disabled:true" style="width: 85px;height: 23px;margin-left:10px">删除</a>
			   			<a id="addInLevelMenu" href="#" class="easyui-linkbutton" data-options="disabled:true" style="width:85px; height:23px;margin-left:10px">添加同级菜单</a>
				    	<a id="addChildLevelMenu" href="#" class="easyui-linkbutton" data-options="disabled:true" style="width:85px; height:23px;margin-left:10px">添加子级菜单</a>
			   			<a id="saveCommit" href="#" class="easyui-linkbutton" data-options="disabled:true" style="width:85px; height:23px;margin-left:10px">提交</a>
			   			<a id="cencel" href="#" class="easyui-linkbutton" data-options="disabled:true" style="width:85px; height:23px;margin-left:10px">取消</a>
			   		</div>
				</form>
		    </div>   
		    <div data-options="region:'center',title:'关联角色'" style="padding:5px;background:#eee;">
		    	<div id="RoleList" style="min-height:415px"></div>
		    </div>   
		</div> 
    </div>  
    
    <!-- 角色列表（不属于某菜单的角色） -->
    <div id="addRoleDialog" class="easyui-dialog" title="添加角色" style="width:600px;height:400px;" data-options="closed:true,iconCls:'icon-save',resizable:true,modal:true">   
   		<table id="RoleList2" style="min-height:340px"></table> 
    </div>
     
</body> 
	
</body>
<!-- JS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/util/md5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sys/manager/menu.js"></script>
</html>