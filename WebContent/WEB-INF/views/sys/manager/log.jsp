<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- CSS -->
<link id="essyuicss" rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/metro-blue/easyui.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/common/common.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/theme-1/theme.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/icon.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/color.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/demo/demo.css" type="text/css" />
<title>Insert title here</title>
</head>
<body>

	<!-- 头部按钮 -->
	<div style="width: 95%;height: 25px;margin-top: 4px;padding-left: 30px; border:0;">
 		<form name="searchFrom" action="">
			用户名   :<input id="username" class="easyui-textbox" data-options="min:0,precision:0" style="width:150px;height: 20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			日志类型    :  <select id="type" class="easyui-combobox" name="type" style="width:200px;" data-options="editable:false,value:''">   
						    <option value="用户登录">用户登录</option>   
						    <option value="动作日志">动作日志</option>   
						</select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			起始时间   :<input  id="startTime"  type= "text" class= "easyui-datebox" data-options="editable:false"/> &nbsp;&nbsp;&nbsp;&nbsp;
		        结束时间   :<input  id="endTime"  type= "text" class= "easyui-datebox" data-options="editable:false"/>
			<a id="search" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin-left:40px">查询</a>
			<a id="reset" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height:23px;margin: 0px 30px">重置</a> 
		</form>
	</div>

	<!-- 日志列表 -->
	<div style="margin-top:50px">
		<table id="LogList" style="min-height:100%;"></table>
	</div>
	
	
</body>
<!-- JS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sys/manager/log.js"></script>
</html>