<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- CSS -->
<link id="essyuicss" rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/metro-blue/easyui.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/common/common.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/theme-1/theme.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/icon.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/color.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/demo/demo.css" type="text/css" />
<title>Insert title here</title>
</head>
<body>
	
	<!-- 头部按钮 -->
	<div data-options="region:'north',title:'角色管理'" style="height:60px; border:0;">
   		<div style="width: 95%;height: 25px;margin-top: 4px;padding-left: 30px; border:0;">
   		    <form name="searchFrom" action="">
				角色名称:<input id="role_name" class="easyui-textbox" data-options="min:0,precision:0" style="width:150px;height: 20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<a id="search" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin: 0px -20px">查询</a>
				<a id="reset" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height:23px;margin: 0px 30px">重置</a> 
			</form>
		</div>
    </div>   
    
    <!-- 添加角色对话框 -->
    <div id="addDialog" class="easyui-dialog" title="添加角色" style="width:400px;height:250px;" data-options="closed:true,iconCls:'icon-save',resizable:true,modal:true">   
	       <form id="RoleForm" method="post" style="width:100%, height:100%, padding:2%">   
			    <div style="margin-top:15px;margin-left:3px">   
			        <label for="name">角色名称:</label>   
			        <input class="easyui-validatebox" type="text" name="name" data-options="required:true,validType:'validateNameLength'" />   
			    </div>   
			    <div style="margin-top:15px;margin-left:3px">   
			        <label for="name">排序:</label>   
			        <input class="easyui-numberbox" type="text" name="ordernum" data-options="required:true" />   
			    </div>  
			    <div style="margin-top:15px;margin-left:3px">   
			        <label for="email">说明:</label>   
			        <input class="easyui-validatebox" type="text" name="info" style="height: 60px; width: 338px;" data-options="multiline:true,validType:'validateInfoLength'" />   
			    </div>  
			    <div style="margin-top:15px;margin-left:3px;text-align:center">
			    	<a id="commitAddRoleBtn" href="#" class="easyui-linkbutton" data-options="" style="width:60px; height:23px;margin-right:5px">提交</a>
			    	<a id="cencel" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin-left:5px">取消</a>
			    </div>
			</form>
	</div>
	

	
	<!-- 角色列表 -->
	<table id="RoleList" style="min-height:690px"></table> 
	
	<!-- 编辑对话框 -->
	<div id="editDialog" class="easyui-dialog" title="添加角色" style="width:550px;height:350px;" style="padding:0px" data-options="closed:true,iconCls:'icon-save',resizable:true,modal:true">   
		<div id="RoleUserEdit" class="easyui-tabs" style="width:100%;height:100%;">
			<div title="基本信息" style="padding:10px;">
				<form id="RoleForm2" method="post" style=" width:90%; height:90%; padding:2%; ">   
					<input class="easyui-validatebox" type="text" id="roleid" name="id" style="display:none;"/>   
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="name">角色名称:</label>   
				        <input class="easyui-validatebox" type="text" id="rolename" name="name" data-options="required:true,validType:'validateNameLength'" />   
				    </div>   
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="ordernum">排序:</label>   
				        <input class="easyui-numberbox" type="text" id="roleordernum" name="ordernum" data-options="required:true" />   
				    </div>  
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="info">说明:</label>   
				        <input class="easyui-validatebox" type="text" id="roleinfo" name="info" style="height: 60px; width: 338px;" data-options="multiline:true,validType:'validateInfoLength'" />   
				    </div>  
				    <div style="margin-top:18px;margin-left:3px;text-align:center">
				    	<a id="commitEditRoleBtn" href="#" class="easyui-linkbutton" data-options="" style="width:60px; height:23px;margin-right:5px">提交</a>
				    	<a id="cencelEdit" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin-left:5px">取消</a>
				    </div>
				</form>  
			</div>
			<div title="角色用户" style="padding:10px;">
				<table id="userList" style="min-height:260px;"></table>
			</div>
			<div title="角色菜单" style="padding:10px;">
				<input id="role_id" type="hidden">
				<div id="menuTree" style="min-height:200px;"></div>
				<div style="margin-top:10px;margin-left:3px;text-align:center">
			    	<a id="commitUpdateMenuBtn" href="#" class="easyui-linkbutton" data-options="" style="width:60px; height:23px;margin-right:5px">提交</a>
			    	<a id="cencelEdit2" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin-left:5px">取消</a>
			    </div>
			</div>
	    </div>
	</div>
	
	<!-- 部门-用户树 -->
	<div id="deptUserTreeDialog" class="easyui-dialog" title="部门用户" style="width:480px;height:400px;" style="padding:0px" data-options="closed:true">
		<div id="UserList2" style="min-height:320px;"></div>
		<div style="margin-top:10px;margin-left:3px;text-align:center">
	    	<a id="commitUpdateRoleUserBtn" href="#" class="easyui-linkbutton" data-options="" style="width:60px; height:23px;margin-right:5px">提交</a>
	    	<a id="cencelEdit3" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin-left:5px">取消</a>
	    </div>
	</div>
	
</body>
<!-- JS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/util/md5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sys/manager/role.js"></script>
</html>