<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- CSS -->
<link id="essyuicss" rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/metro-blue/easyui.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/common/common.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/theme-1/theme.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/icon.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/color.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/demo/demo.css" type="text/css" />
<title>Insert title here</title>
</head>
<body>
	
	<!-- 头部按钮 -->
	<div data-options="region:'north',title:'用户管理'" style="height:60px; border:0;">
   		<div style="width: 95%;height: 25px;margin-top: 4px;padding-left: 30px; border:0;">
   		    <form name="searchFrom" action="">
				用户名   :<input id="user_username" class="easyui-textbox" data-options="min:0,precision:0" style="width:150px;height: 20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				帐号    :<input id="user_loginname" class="easyui-textbox" data-options="min:0,precision:0" style="width:150px;height: 20px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				<a id="search" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin: 0px -20px">查询</a>
				<a id="reset" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height:23px;margin: 0px 30px">重置</a> 
			</form>
		</div>
    </div>   
    
	<!-- 用户列表 -->
	<table id="UserList" style="min-height:100%"></table> 
	
	<!-- 编辑对话框 -->
	<div id="editDialog" class="easyui-dialog" title="编辑用户" style="width:550px;height:500px;" style="padding:0px" data-options="closed:true,iconCls:'icon-save',resizable:true,modal:true">   
		<div id="RoleUserEdit" class="easyui-tabs" style="width:100%;height:100%;">
			<div title="基本信息" style="padding:10px;">
				<form id="UserForm" method="post" style=" width:90%; height:90%; padding:2%; ">   
					<!-- 记录当前编辑的用户id -->
					<input type="hidden" id="user_id" name="id">
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="name">用户名:</label>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
				        <input class="easyui-validatebox" type="text" id="edit_username" name="userName" data-options="required:true" />   
				    </div>   
				    <div style="margin-top:18px;margin-left:3px">
				        <label for="ordernum">用户帐号:</label>   &nbsp;&nbsp;
				        <input class="easyui-validatebox" type="text" id="edit_loginname" name="loginName" data-options="required:true" />   
				    </div>  
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="info">性别:</label>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				        <select  class="easyui-combobox" id="edit_gender" name="gender" data-options="required:true" >
					        <option value="男">男</option>   
	    					<option value="女">女</option>   
				        </select>   
				    </div>
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="info">手机号码:</label>   &nbsp;&nbsp;
				        <input class="easyui-validatebox" type="text" id="edit_mobile" name="mobile" data-options="required:true" />   
				    </div> 
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="info">座机:</label>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				        <input class="easyui-validatebox" type="text" id="edit_telephone" name="telephone" data-options="required:true" />   
				    </div>  
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="info">邮箱:</label>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				        <input class="easyui-validatebox" type="text" id="edit_mail" name="mail" data-options="required:true" />   
				    </div>  
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="info">出生日期:</label>   &nbsp;&nbsp;
				        <input type= "text" class= "easyui-datebox" style="margin-left:70px" id="edit_birthday" name="birthday" data-options="required:true" />
				    </div>
				    <div style="margin-top:18px;margin-left:3px">   
				        <label for="info">地址:</label>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				        <input class="easyui-validatebox" type="text" id="edit_address" name="address" style="width:300px"/>   
				    </div>
				    <div style="margin-top:22px;margin-left:3px;margin-bottom:10px;text-align:center">
				    	<a id="commitEditRoleBtn" href="#" class="easyui-linkbutton" data-options="" style="width:60px; height:23px;margin-right:5px">提交</a>
				    	<a id="cencelEdit" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin-left:5px">取消</a>
				    </div>
				</form>  
			</div>
			
			<div title="用户角色" style="padding:10px;">
				<table id="RoleList" style="min-height:400px;"></table>
			</div>
	</div>
	<!-- 添加用户 -->
	<div id="addDialog" class="easyui-dialog" title="添加用户" style="width:550px;height:600px;" style="padding:0px" data-options="closed:true,iconCls:'icon-save',resizable:true,modal:true">   
		<div>
			<form id="UserForm2" method="post" style=" width:90%; height:90%; padding:2%; ">   
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="name">用户名:</label>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			        <input class="easyui-validatebox" type="text" id="add_username" name="userName" data-options="required:true" />   
			    </div>   
			    <div style="margin-top:18px;margin-left:3px">
			        <label for="ordernum">用户帐号:</label>   &nbsp;&nbsp;
			        <input class="easyui-validatebox" type="text" id="add_loginname" name="loginName" data-options="required:true" />   
			    </div>  
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="name">密码:</label>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			        <input class="easyui-validatebox" type="text" id="add_password" name="password" data-options="required:true" />   
			    </div> 
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="name">重复密码:</label>  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
			        <input class="easyui-validatebox" type="text" id="add_repassword" name="repassword" data-options="required:true" />   
			    </div> 
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="info">性别:</label>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			        <select  class="easyui-combobox" id="add_gender" name="gender" data-options="required:true" >
				        <option value="男">男</option>   
    					<option value="女">女</option>   
			        </select>   
			    </div>
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="info">手机号码:</label>   &nbsp;&nbsp;
			        <input class="easyui-validatebox" type="text" id="add_mobile" name="mobile" data-options="required:true" />   
			    </div> 
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="info">座机:</label>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			        <input class="easyui-validatebox" type="text" id="add_telephone" name="telephone" data-options="required:true" />   
			    </div>  
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="info">邮箱:</label>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			        <input class="easyui-validatebox" type="text" id="add_mail" name="mail" data-options="required:true" />   
			    </div>  
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="info">出生日期:</label>   &nbsp;&nbsp;
			        <input type= "text" class= "easyui-datebox" style="margin-left:70px" id="add_birthday" name="birthday" data-options="required:true" />
			    </div>
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="info">所属部门:</label>   &nbsp;&nbsp;
			        <select id="add_deptNames" class="easyui-combobox" name="deptNames" style="width:400px;"></select> 
			    </div>
			    <div style="margin-top:18px;margin-left:3px">   
			        <label for="info">地址:</label>   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			        <input class="easyui-validatebox" type="text" id="add_address" name="address" style="width:450px"/>   
			    </div>
			    <div style="margin-top:22px;margin-left:3px;margin-bottom:10px;text-align:center">
			    	<a id="commitAddRoleBtn" href="#" class="easyui-linkbutton" data-options="" style="width:60px; height:23px;margin-right:5px">提交</a>
			    	<a id="cencelAdd" href="#" class="easyui-linkbutton" data-options="" style="width: 60px;height: 23px;margin-left:5px">取消</a>
			    </div>
			</form>  
		</div>
   	</div>
   </div>
   
   <!-- 角色列表（不属于某用户的角色） -->
   <div id="addRoleDialog" class="easyui-dialog" title="添加角色" style="width:550px;height:380px;" data-options="closed:true,iconCls:'icon-save',resizable:true,modal:true">
   	   <table id="RoleList2" style="min-height:340px"></table> 
   </div>
   
	
</body>
<!-- JS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/util/md5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sys/manager/user.js"></script>
</html>