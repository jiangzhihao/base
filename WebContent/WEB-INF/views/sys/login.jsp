<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- CSS -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/icon.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/color.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/default/easyui.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/common/common.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/sys/login.css" type="text/css" />
<title>${systemname}</title>
</head>
<body class="bg" style="background-image: url('${pageContext.request.contextPath}/images/sys/login/bg.png')">

	<div class="wrap">
	    <div class="yy"></div> 
	    <div class="loginbox" style="background: url('${pageContext.request.contextPath}/images/sys/login/login_bg.png'),no-repeat">
	   	    <div class="login-c">
	   	    	  <div><span>用户名 &nbsp;&nbsp;</span><input type="text" class="txtb" id="LoginName"></div>
	   	    	  <div><span>密&nbsp;&nbsp;&nbsp;码</span><input type="password" class="txtb" id="pwd"></div>
	   	    	  <input type="button" value="登 录" class="logbtn" onclick="GoLogin()"/>
	   	    </div>	
	   </div>
	</div>
	
</body>
<!-- JS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sys/login.js"></script>
</html>