<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>




	<!-- 表单实现文件上传 -->
	<!-- 为了能上传文件，必须将表单的method设置为POST，并将enctype设置为multipart/form-data。只有在这样的情况下，浏览器才会把用户选择的文件以二进制数据发送给服务器 -->
	<form action="${pageContext.request.contextPath}/file/upload"
		method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td>文件描述:</td>
				<td><input type="text" name="description"></td>
			</tr>
			<tr>
				<td>请选择文件:</td>
				<td><input type="file" name="file"></td>
			</tr>
			<tr>
				<td><input type="submit" value="上传"></td>
			</tr>
		</table>
	</form>
	
	
	
	
	<!-- Ajax实现文件上传 -->
	<br />
	<h3>Ajax实现文件上传</h3>
	<input type="file" id="myfile">
	<button id="upload">上传</button>
	
	
	
	
	<!-- AjaxFileUpload实现文件上传 -->
	<br />
	<h3>AjaxFileUpload实现文件上传</h3>
	<input type="file" id="myfile2" name="file">
	<button id="ajaxfileupload">上传</button>




	<!-- 已上传文件列表 -->
	<br />
	<h3>已经上传的文件</h3>
	<c:forEach var="file" items="${files}">
        	文件名：<a href="${pageContext.request.contextPath}/file/download2?id=${file.FILE_ID}">${file.FILE_NAME}</a>  ${file.UPLOAD_TIME } <br/>                     
    </c:forEach>
</body>



<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/commonjs/ajaxfileupload.js"></script>
<script type="text/javascript">

	$("#ajaxfileupload").click(function() {
		var basePath = "${pageContext.request.contextPath}";
		var url = basePath+"/file/upload2";
		$.ajaxFileUpload({  
		    type: "post",  
		    url: url,  
		    data:{time : new Date()},//要传到后台的参数，没有可以不写  
		    fileElementId:'myfile2',//文件选择框的id属性  
		    dataType: 'json',//服务器返回的格式  
		    async : false,  
		    success: function(data){  
		        if(data.result=='SUCCESS'){  
		            alert("上传成功");  
		        }else{  
		        	alert("上传失败");  
		        }  
		    },  
		    error: function (data, status, e){  
		    	alert("上传失败");  
		    }  
		});  
	});
	$("#upload").click(function() {
		var basePath = "${pageContext.request.contextPath}";
		var url = basePath+"/file/upload2";
		var formData = new FormData();
		formData.append("file",$("#myfile")[0].files[0]);
		$.ajax({ 
			url : url, 
			type : 'POST', 
			data : formData,
			dataType : 'json',
			// 告诉jQuery不要去处理发送的数据
			processData : false, 
			// 告诉jQuery不要去设置Content-Type请求头
			contentType : false,
			beforeSend:function(){
				console.log("正在进行，请稍候");
			},
			success : function(data) { 
				if(data.result == 'SUCCESS'){
					console.log("成功"+data.message);
				}else{
					console.log("失败");
				}
			}, 
			error : function(data) { 
				console.log("error");
			} 
		});
	});
 
</script>
</html>