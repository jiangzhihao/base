<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<!-- CSS -->
<link id="essyuicss" rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/metro-blue/easyui.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/common/common.css" type="text/css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resource/css/themes/theme-1/theme.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/icon.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/themes/color.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/easyui/demo/demo.css" type="text/css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/sys/main.css" type="text/css" />
<title>${SYSTEM_PROP.systemName}</title>
</head>
<body>

<div class="easyui-layout" data-options="fit:'true'">
	
	<div class="silder">
		<ul>
			<li>个人中心</li>
			<li onclick="Logout()">退出账号</li>
		</ul>
	</div>
	
	<!-- 上 -->
	<div data-options="region:'north'" style="background-image:'url({pageContext.request.contextPath}/images/sys/main/header_bg.png');height: 67px;border:none;overflow:hidden;margin-bottom: 3px">
		<div class="titlebg">
			<div style="float: left; margin: 0 0 0 20px; line-height: 67px;">
				&nbsp;<img src="${pageContext.request.contextPath}/images/sys/main/title.png">
			</div>
			<div class="adminmy" style="float: right; text-align: right; line-height: 67px; padding-right: 8px; font-size: 15px;">
				<a href="#">
					<img src="${pageContext.request.contextPath}/images/sys/main/userhead.png" style="margin-left: 15px; margin-right: 5px; border-style: none; vertical-align: middle;">
						<span style="font-family: 'Microsoft YaHei'; font-size: 14px; color: #ffffff; margin-right: 8px">${LOGIN_USER.userName}</span>
				</a>
				<span>
					<img src="${pageContext.request.contextPath}/images/sys/main/arrow_down.png">
				</span>
			</div>
		</div>
	</div>
	
	<!-- 左侧菜单 -->
	<div data-options="region:'west'" style="width: 230px; border: 0; margin-left: 2px; margin-right: 2px;">
		<div id="menus_div" class="easyui-accordion" data-options="fit:true,border:false,animate:true"></div>
	</div>
	
	<!-- 中间 -->
	<div data-options="region:'center'" style="border: 0; background: #e4f6f0;">
		<div id="tabpaper" style="width: 100%; height: 100%;">
		</div>
	</div>
	
	<!-- 页脚 -->
	<div data-options="region:'south'" style="height: 32px; border: 0; font-size: 14px; overflow: hidden;">
		<div style="width: 100%;height: 30px;background:url('${pageContext.request.contextPath}/images/sys/main/footer.png') repeat-x;position: relative; ">
			<div style="text-align: center; color: #FDFBFB; line-height: 30px;">
					${SYSTEM_PROP.footerMsg}小组办公室 Copyright © 2017-2020 
					<span onclick="location.href='#'" class="fa-span" style="position: absolute; right: 10px; top: 5px">
					<i class="fa-question-sign fa-1x"></i>
					</span>
			</div>
		</div>
	</div>
	
	<div id="tabPrivMenu" class="easyui-menu">
		<div onclick="closeTabs('tabpaper','refresh')" data-options="iconCls:'icon-reload'">刷新当前</div>
		<div onclick="closeTabs('tabpaper','other')">关闭其他</div>
		<div onclick="closeTabs('tabpaper','all')">关闭全部</div>
		<div class="menu-sep"></div>
		<div onclick="closeTabs('tabpaper','right')">关闭右侧标签页</div>
		<div onclick="closeTabs('tabpaper','left')">关闭左侧标签页</div>
	</div>
	
</div>


<!-- JS -->
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/jquery.easyui.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/easyui/locale/easyui-lang-zh_CN.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/common/common.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/util/md5.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/sys/main.js"></script>
<script type="text/javascript" charset="utf-8">
	$(function(){
	    $('.adminmy').click(function(){
		     $('.silder').show();						  
		   })
		  $('.silder').mouseleave(function(){
		     $('.silder').hide();								
										
		  });
		  
	})
	$(document).bind("click",function(e){
	var target=$(e.target);
	if(target.closest(".adminmy").length==0 && target.closest(".silder").length == 0){
	     $('.silder').hide();		
	}
})
</script>
</body>
</html>