var basePath;

var bookCoverPath;


$(function() {
	var curPath = window.document.location.href;
    //获取主机地址之后的目录，如： test/test.jsp
    var pathName = window.document.location.pathname;
    var pos = curPath.indexOf(pathName);
    //获取主机地址，如： http://localhost:8088
    var basePath_ = curPath.substring(0, pos);
  //获取带"/"的项目名，如：/test
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    basePath = basePath_+projectName;
    bookCoverPath = basePath + '/images/bookcover/';
    
    initEasyUi();
})


function add_css(str_css) {
	var node = document.createElement('style');
	node.type='text/css';  
	if(node.styleSheet){
		node.styleSheet.cssText = str_css;  
	}else {  
		node.innerHTML = str_css;       
	}  
	document.getElementsByTagName('head')[0].appendChild(node); 
}

/**
 * 该函数用于格式化日期，它有一个'date'参数并且会返回一个字符串类型的值。
 * 下面作用是重写默认的formatter函数。
 */
function initEasyUi() {
	$.fn.datebox.defaults.formatter = function(date){
		var y = date.getFullYear();
		var m = date.getMonth()+1;
		var d = date.getDate();
		return m+'-'+d+'-'+y;
	}
}



/**
 * 长度校验器
 */
$.extend($.fn.validatebox.defaults.rules, {    
    validateNameLength: {    
        validator: function(value,param){   
            if(null != value) {
            	if(value.length >= 4 && value.length <= 16) {
            		return true;
            	}
            }
            return false;
        },    
        message: '角色名称长度为4至16'   
    }    
});  
$.extend($.fn.validatebox.defaults.rules, {    
    validateInfoLength: {    
        validator: function(value,param){   
            if(null != value) {
            	if(value.length >= 5 && value.length <= 300) {
            		return true;
            	}
            }
            return false;
        },    
        message: '角色名称长度为5至300'   
    }    
}); 


