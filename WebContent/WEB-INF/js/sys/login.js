
$(function() {
	$("#LoginName").focus();
});

/**
 * 登录事件
 */
function GoLogin() {
	var LoginName = $('#LoginName').val();
	var password = $('#pwd').val();
	if (checkMessage(LoginName, password)) {
		$.ajax({
			url : basePath + "/sys/user/authe",
			type : 'post',
			dataType : "json",
			data : {
				"loginname" : LoginName,
				"password" : password
			},
			error : OnError,
			success : function(data) {
				if(data.result == 'SUCCESS') {
					window.location = basePath + "/sys/page/main";
				} else {
					var errorMsg = data.message;
					if(null == errorMsg || errorMsg.length <= 0) {
						errorMsg = "登录失败";
					}
					$.messager.alert("提示", "用户名或密码错误，请您重试！", "warning");
				}
			}
		});
	}
}


/**
 * 检测登录信息是否为空
 */
function checkMessage(loginname, password) {
	if ($.trim(LoginName) == "") {
		$.messager.alert("提示", "用户名不能为空！", "warning");
		$('#LoginName').focus();
		return false;
	} else if ($.trim(password) == "") {
		$.messager.alert("提示", "密码不能为空！", "warning");
		$('#pwd').focus();
		return false;
	} else {
		return true;
	}

}

/**
 * 获取键盘回车事件
 */
$(document).keyup(
		function(event) {
			if (event.keyCode == 13) {
				var loginName = $('#LoginName').val();
				var password = $('#pwd').val();
				var ycode = $('#veryCode').val();
				if ($.trim(loginName) != '' && $.trim(password) != ''
						&& $('.messager-window').length == 0) {
					GoLogin();
				} else if ($.trim(loginName) == '') {
					$('#LoginName').focus();
				} else if ($.trim(loginName) != '' && $.trim(password) == '') {
					$('#pwd').focus();
				}
			}
		}
);

//异常处理
function OnError(XMLHttpRequest, textStatus, errorThrown) {
	if (errorThrown || textStatus == "error" || textStatus == "parsererror"
			|| textStatus == "notmodified") {
		alert("请求数据时发生错误！");
		return;
	}
	if (textStatus == "timeout") {
		alert("请求数据超时！");
		return;
	}
}

