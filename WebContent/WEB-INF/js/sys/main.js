var menu_tree_url = '/base/sys/menu/getMenuByUsers';

/*
 * ************** 组件说明
 * 
 * ID            组件               作用
 * tabpaper      div		再center中展示的Tab页
 * menus_div     div		左侧菜单
 * 
 */

$(function() {
	initMenuTree();
	initTab();
})

/**
 * 初始化系统菜单
 */
function initMenuTree() {
	var firstMenuItem;
	var args = {time : new Date()};
	$.ajax({
		url : menu_tree_url,
		type : 'post',
		dataType : "json",
		data : args,
		success : function(data) {
			if(data.result == 'SUCCESS') {
				var menuList = data.object;
				if(null != menuList && menuList.length > 0) {
					for(var i=0;i<menuList.length;i++) {
						var css_str = '';
						var menu = '<div class="menulist">';
						var childs = menuList[i].childs;
						if(null != childs && childs.length > 0) {
							if(i == 0) {
								firstMenuItem = childs[0];
							}
							for(var j=0;j<childs.length;j++) {
								var child = childs[j];
								menu +='<div class="ellipsis" name="'+child.name+child.id+'" parentindex="'+i+'" onmouseover="childMenuMouseOver(this)" onmouseout="childMenuMouseOut(this)" onclick="openTab('+child.id+',\'' + child.name + '\',\'' + child.url + '\',this)">'+
								'<img class="childmenu_img" src="'+basePath+'/images/sys/menu/off/'+child.icon+'" style="width:16px;height:16px;display:none" />'+
								'<img class="childmenu_img_hover" src="'+basePath+'/images/sys/menu/on/'+child.icon+'" onerror="this.src=\''+basePath+'/images/sys/menu/on/'+child.icon+'\';this.onerror=null" style="width:16px;height:16px;display:none" />'+
								'<span>'+child.name+'</span>'+
								'</div>';
							}
						}
						menu += "</div>";
						css_str +=".leftMenuIcon" + i + "{height:20px;width:20px;left: 10%;vertical-align: middle;} ";
						$('#menus_div').accordion('add', {
							title : menuList[i].name,
							iconCls : 'leftMenuIcon' + i,
							content : menu,
							selected : false
						});
						$('.leftMenuIcon'+i).append('<img src="'+basePath+'/images/sys/menu/off/'+ menuList[i].icon +'" style="height:100%;width:100%; " class="heiicon"/><img src="'+basePath+'/images/sys/menu/on/'+ menuList[i].icon +'" style="height:100%;width:100%; display:none " class="lanicon" />');
					}
					add_css(css_str);
					$('#menus_div').accordion('select',0);
					openTab(
							firstMenuItem.id
							, firstMenuItem.name
							, firstMenuItem.url
							, $('#menus_div').find('.menulist').first().children('.ellipsis').first()
							,{type:'getMenu'}
					);
				}
			}
		}
	});
}

function initTab() {
	$('#tabpaper').tabs({
		onContextMenu : function(e, title, index) {
			e.preventDefault();
			$('#tabPrivMenu').menu('show', {
				left : e.pageX,
				top : e.pageY
			});

			$(this).tabs('select', index);
			
			$('.tabs li:eq('+index+') .tabs-inner').click();
		},
		onClose : function(title,index){
			
			//关闭标签页,取消菜单选中状态
			var obj = $('<div></div>');
			obj.html(title);
			unClickChildMenuStyle($('.ellipsis[name="'+obj.text()+'"]'));
		},
		onAdd : function(title,index){
			
			//点击标签页,对应菜单设置为选中状态
			$('.tabs li:eq('+index+') .tabs-inner').click(function(){
				var tabname = $(this).find('.tabs-title').text();
				var obj = $('.ellipsis[name="'+tabname+'"]');
				$('#menus_div').accordion('select',parseInt(obj.attr('parentindex')));//展开父级菜单
				clickChildMenuStyle(obj)
			});
		}
	});
}


/**
 * 
 * @param menuItem
 */
function childMenuMouseOver(menuItem) {
	menuItem = $(menuItem);
	menuItem.addClass('divhover');
	menuItem.children('.childmenu_img').hide();
	menuItem.children('.childmenu_img_hover').hide();
}

/**
 * 
 * @param menuItem
 */
function childMenuMouseOut(menuItem) {
	menuItem = $(menuItem);
	menuItem.removeClass('divhover');
	if(menuItem.hasClass('divclick')==false){
		menuItem.children('.childmenu_img').hide();
		menuItem.children('.childmenu_img_hover').hide();
	}
}

/**
 * 设置子菜单点击样式
 * @param object
 */
function clickChildMenuStyle(object){
	var obj = $(object);
	if(obj.hasClass('divclick')==false){
		var firstClick = $('.menulist>.divclick');
		firstClick.children('.childmenu_img_hover').hide();
		firstClick.children('.childmenu_img').hide();
		firstClick.removeClass('divclick');
		firstClick.parent().removeClass('panelon');
		obj.addClass('divclick');
		obj.parent().addClass('panelon');
		obj.children('.childmenu_img_hover').hide();
		obj.children('.childmenu_img').hide();
	}
}

/**
 * 移除子菜单点击样式
 * @param object
 */
function unClickChildMenuStyle(object){
	if(object.hasClass('divclick')){
		object.removeClass('divclick');
		object.children('.childmenu_img_hover').hide();
		object.children('.childmenu_img').hide();
	}
}

/**
 * 展开一个Tab页
 * @param tabid
 * @param title
 * @param uri
 * @param object
 */
function openTab(tabid, title, uri, object) {
	clickChildMenuStyle(object);
	title = title + '<span style="display:none">' + tabid + '</span>';
	if ($('#tabpaper').tabs('exists', title)) {
		$('#tabpaper').tabs('select', title);
	} else {
		if (uri != null && uri != '') {
			if (new RegExp(/^\//).test(uri)) {
				uri = basePath + uri;
			} else if (new RegExp(/^http:/).test(uri.toLowerCase()) == false) {
				uri = "http://" + uri;
			}
		}
		$('#tabpaper').tabs('add',{
			id : tabid,
			// tabWidth : 100,
			title : title,
			closable : true,
			content : '<iframe id="tabifram'
					+ tabid
					+ '" name="tabiframe" scrolling="auto" frameborder="0"  src="'
					+ uri
					+ '" style="width:100%;height:99.5%;"></iframe>'
		});
	}
}

/**
 * 关闭Tab页，有多种关闭方式
 */
var closeTabs = function(tabId, type) {
	var index = $('#' + tabId).tabs('getTabIndex', $('#' + tabId).tabs('getSelected'));
	var tabCount = $('#' + tabId).tabs('tabs').length;
	switch (type) {
	case 'refresh':
		var tab = $('#' + tabId).tabs('getSelected');
		var ifram = $('#tabifram'+tab.panel('options').id);
		ifram.attr('src',ifram.attr('src'));
		break;
	case 'all':
		for ( var i = tabCount - 1; i >= 0; i--) {
			$('#' + tabId).tabs('close', i);
		}
		break;
	case 'other':
		for ( var i = tabCount - 1; i > index; i--) {
			$('#' + tabId).tabs('close', i);
		}

		var num = index - 1;
		for ( var i = 0; i <= num; i++) {
			$('#' + tabId).tabs('close', 0);
		}
		break;
	case 'right':
		for ( var i = tabCount - 1; i > index; i--) {
			$('#' + tabId).tabs('close', i);
		}
		break;
	case 'left':
		var num = index - 1;
		for ( var i = 0; i <= num; i++) {
			$('#' + tabId).tabs('close', 0);
		}
		break;
	}
};

function Logout() {
	window.location.href=basePath + '/sys/user/logout';
}












