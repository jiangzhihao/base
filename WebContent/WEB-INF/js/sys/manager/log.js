


$(function() {
	
	initLogList();
	
	initBtnClick();
	
})

/**
 * 初始化日志列表
 */
function initLogList() {
	var url = basePath + '/sys/log/getLogs';
	var args = {
			time : new Date()
	};
	$("#LogList").datagrid({
		url: url,
	    method : 'post',
	    rownumbers : true,
	    singleSelect : true,
	    queryParams : args,
	    columns:[[   
	        {field:'type',title:'日志类型',width:'15%',align:'center'},    
	        {field:'description',title:'动作描述',width:'28%',align:'center'},    
	        {field:'user',title:'操作用户',width:'15%',align:'center', formatter : function(value, obj,index) {
				return  obj.user.userName;
	        }} ,
	        {field:'time',title:'发生时间',width:'15%',align:'center', formatter : function(value, obj,index) {
	        	var year = value.year+"-";
	        	year = year.substring(1,year.length);
	        	return "20"+year+(value.month+1)+"-"+value.date+" "+value.hours+":"+value.minutes+":"+value.seconds;
	        }} ,
	        {field:'method',title:'代码定位',width:'12%',align:'center'},
	        {field:'ip',title:'请求IP',width:'12%',align:'center'},
	    ]]  ,
	    pagination : true,
	    pageSize : 15,
	    pageList : [10, 15, 20, 25],
		onLoadSuccess : function() { //成功加载后的事件
		}
	});
}



/**
 * 初始化按钮点击事件
 */
function initBtnClick() {
	$("#search").click(function() {
		search();
	});
	$("#reset").click(function() {
		reset();
	});
}

/**
 * 条件查询
 */
function search() {
	var userName = $("#username").textbox("getValue");
	var type = $("#type").combobox("getValue");
	var startTime = $("#startTime").datebox("getValue");
	var endTime = $("#endTime").datebox("getValue");
	var args = {
		time : new Date(),
		userName : userName,
		type : type,
		startTime : startTime,
		endTime : endTime
	}
	$('#LogList').datagrid({
		queryParams : args
	});
	$("#LogList").datagrid('reload');
}

function reset() {
	$("#type").combobox("unselect");
	$("#username").textbox("setValue", "");
	$("#username").textbox("setText", "");
	$("#startTime").textbox("setValue", "");
	$("#startTime").textbox("setText", "");
	$("#endTime").textbox("setValue", "");
	$("#endTime").textbox("setText", "");
	$('#LogList').datagrid({
		queryParams : {
			time : new Date()
		}
	});
	$("#LogList").datagrid('reload');
	
}