
$(function() {
	
	initRoleList();
	initBtnClick();
	
})

function initBtnClick() {
	$("#commitAddRoleBtn").click(function() {
		$('#RoleForm').form('submit', {    
			url: basePath + '/sys/role/add',
		    onSubmit: function(){    
		        // do some check    
		        // return false to prevent submit;    
		    },    
		    success:function(data){ 
		    	data = eval("("+data+")");
		    	$.messager.show({
					title:'提示',
					msg:data.message,
					timeout:5000,//0为不消失
					width : 300,
					height : 100,
					showType:'slide',
				}); 
		    	$('#RoleForm').form('clear');
		    	$('#addDialog').dialog('close');
		    	reloadRoleList();   
		    }    
		});  
	});
	$("#commitEditRoleBtn").click(function() {
		$("#RoleForm2").form('submit', {    
			url: basePath + '/sys/role/update',
		    onSubmit: function(){    
		        // do some check    
		        // return false to prevent submit;    
		    },    
		    success:function(data){ 
		    	data = eval("("+data+")");
		    	$.messager.show({
					title:'提示',
					msg:data.message,
					timeout:5000,//0为不消失
					width : 300,
					height : 100,
					showType:'slide',
				}); 
		    	$('#RoleForm2').form('clear');
		    	$('#editDialog').dialog('close');
		    	reloadRoleList();   
		    }    
		});  
	});
	$("#commitUpdateMenuBtn").click(function() {
		commitUpdateRoleMenu();
	});
	$("#commitUpdateRoleUserBtn").click(function() {
		var roleId = $("#role_id").val();
		addUsersToRole(roleId);
	});
	$("#cencel").click(function() {
		$('#RoleForm').form('clear');
		$("#addDialog").dialog('close');
	});
	$("#cencelEdit").click(function() {
		$('#RoleForm2').form('clear');
		$("#editDialog").dialog('close');
		$("#roleId").val("");
	});
	$("#cencelEdit2").click(function() {
		$('#RoleForm2').form('clear');
		$("#editDialog").dialog('close');
		$("#roleId").val("");
	});
	$("#cencelEdit3").click(function() {
		$("#deptUserTreeDialog").dialog('close');
		$("#UserList2").datagrid("reload");
		$("#roleId").val("");
	});
	$("#search").click(function() {
		var key = $("#role_name").textbox("getText");
		reloadRoleList(key);
	});
	$("#reset").click(function() {
		$("#role_name").textbox("setText", "");
		reloadRoleList();
	});
}


/**
 * 初始化角色列表
 */
function initRoleList() {
	$('#RoleList').datagrid({    
	    url: basePath + '/sys/role/list',  
	    method : 'post',
	    rownumbers : true,
	    singleSelect : false,
	    queryParams : {
	    	name : ""
	    },
	    columns:[[   
	        {field:'ck',align : 'center',checkbox:true, hidden:true}, 
	        {field:'name',title:'角色名称',width:'28%',align:'center'},    
	        {field:'info',title:'简介',width:'50%',align:'center'},    
	        {field:'id',title:'操作',width:'18%',align:'center', formatter : function(value, obj,index) {
				return  '<div style="cursor:pointer;width:50px;margin:0px auto"' 
				+ 'onclick="editRole('+obj.id+','+index+')"><img src='+basePath+'/images/app/btn_edit.png'
				+ ' style="vertical-align: middle;" title="编辑" /><span class="">编辑</span></div>';
	        }}    
	    ]]  ,
	    pagination : true,
	    pageSize : 15,
	    pageList : [10, 15, 20 ,25],
	    toolbar : [
			   		{
			   			text : '添加',
			   			iconCls : 'icon-add',
			   			handler : function() {
			   				$("#addDialog").dialog('open');
			   			}
			   		},
			   		{
			   			text : '删除',
			   			iconCls : 'icon-remove',
			   			handler : function() {
			   				activState();
			   			}
			   		},
			   		{
			   			text : '提交',
			   			iconCls : 'icon-save',
			   			disabled : true,
			   			id:"submit",
			   			handler : function() {
			   				delRoles();
			   			}
			   		},
			   		{
			   			text : '取消',
			   			iconCls : 'icon-cancel',
			   			disabled : true,
			   			id:"cancel",
			   			handler : function() {
			   				initState();
			   			}
			   		}
				],
		onLoadSuccess : function() { //成功加载后的事件
		}
	});  
}

/**
 * 初始化用户列表
 */
function initUserList(id) {
	$("#userList").datagrid({    
	    url: basePath + '/sys/userrole/list',  
	    method : 'post',
	    rownumbers : true,
	    queryParams : {
	    	roleId : id
	    },
	    columns:[[   
	        {field:'ck',align : 'center',checkbox:true}, 
	        {field:'userName',title:'用户名',width:'110px',align:'center'},    
	        {field:'loginName',title:'登录名',width:'120px',align:'center'}, 
	        {field:'telephone',title:'联系方式',width:'120px',align:'center'},    
	        {field:'id',title:'操作',width:'100px',align:'center', formatter : function(value, obj,index) {
				return  '<div style="cursor:pointer;width:50px;margin:0px auto"' 
				+ 'onclick="removeRoleUsers('+obj.id+')"><img src='+basePath+'/images/app/btn_edit.png'
				+ ' style="vertical-align: middle;" title="移除" /><span class="">移除</span></div>';
	        }}    
	    ]]  ,
	    pagination : true,
	    pageSize : 15,
	    pageList : [10, 15, 20 ,25],
	    toolbar : [
			   		{
			   			text : '添加',
			   			iconCls : 'icon-add',
			   			handler : function() {
			   				initUserList2(id);
			   				$("#deptUserTreeDialog").dialog('open');
			   			}
			   		},
			   		{
			   			text : '移除',
			   			iconCls : 'icon-remove',
			   			handler : function() {
			   				removeRoleUsers();
			   			}
			   		},
				],
		onLoadSuccess : function() { //成功加载后的事件
		}
	});  
}

/**
 * 初始化用户表（不属于该角色的用户）
 * @param id
 */
function initUserList2(id) {
	$("#UserList2").datagrid({
		url: basePath + '/sys/user/notinrole/'+id,  
	    method : 'post',
	    rownumbers : true,
	    queryParams : {
	    	time : new Date()
	    },
	    columns:[[   
	        {field:'ck',align : 'center',checkbox:true}, 
	        {field:'userName',title:'用户名',width:'110px',align:'center'},    
	        {field:'telephone',title:'联系方式',width:'130px',align:'center'},    
	        {field:'deptNames',title:'所属部门',width:'150px',align:'center'}    
	    ]] ,
	    pagination : true,
	    pageSize : 15,
	    pageList : [10, 15, 20 ,25]
	});
}

/**
 * 初始化菜单树
 */
function initMenuTree(roleId) {
	$("#role_id").val(roleId);
	$('#menuTree').tree({    
	    url: basePath + '/sys/menu/tree/'+roleId,   
	    onlyLeafCheck : false,
	    checkbox : true,
	    lines : true
	});  
}

/**
 * 为用户添加角色
 * @param roleId
 */
function addUsersToRole(roleId) {
	var selects = $('#UserList2').datagrid('getSelections');
	if(selects.length <= 0) {
		alert("请先选择要删除的角色");
		return ;
	} else {
		var ids = "";
		var num = 0;
		for(var i=0;i<selects.length;i++) {
			ids = ids + selects[i].id +',';
			num ++;
		}
		ids = ids.substring(0, ids.length-1);
		var url = basePath + "/sys/userrole/add";
		var args = {
				ids : ids, 
				roleId : roleId, 
				num : num
		}
		$.ajax({
			url : url ,
			data : args, 
			method : 'post',
			dataType : "json",
			success : function(data) {
				$.messager.show({
					title:'提示',
					msg:data.message,
					timeout:5000,//0为不消失
					width : 300,
					height : 100,
					showType:'slide',
				}); 
		    	$("#deptUserTreeDialog").dialog("close");
		    	$("#userList").datagrid("reload");
			}
		});
	}
}

/**
 * 批量删除
 */
function delRoles() {
	var selects = $('#RoleList').datagrid('getSelections');
	if(selects.length <= 0) {
		alert("请先选择要删除的角色");
		return ;
	} else {
		var ids = "";
		var num = 0;
		for(var i=0;i<selects.length;i++) {
			ids = ids + selects[i].id +',';
			num ++;
		}
		ids = ids.substring(0, ids.length-1);
		var args = {
				time : new Date(), 
				ids : ids,
				num : num
		}
		$.ajax({
			url : basePath + '/sys/role/delete',
			data : args,
			method : 'post',
			success : function (data) {
				data = eval("("+data+")");
				$.messager.show({
					title:'提示',
					msg:data.message,
					timeout:5000,//0为不消失
					width : 300,
					height : 100,
					showType:'slide',
				}); 
		    	reloadRoleList();
			}
		});
	}
}

function getMenuTreeSelects() {
	var ids = '';
	var roots = $('#menuTree').tree('getRoots');
	if(null != roots && roots.length > 0) {
		for(var i=0;i<roots.length;i++) {
			var childs = roots[i].children;
			if(null != childs && childs.length > 0) {
				for(var z=0;z<childs.length;z++) {
					var nowChild = $('#menuTree').tree('find', childs[z].id);
					if(null != nowChild) {
						childs[z].checked = nowChild.checked;
					}
				}
				for(var j=0;j<childs.length;j++) {
					var child = childs[j];
					if(child.checked) {
						ids = ids + child.id + ',';
					}
				}
			}
		}
	}
	if(ids.length > 0) {
		ids = ids.substring(0, ids.length-1);
	}
	return ids;
}

/**
 * 初始化编辑界面
 * @param id 角色ID
 * @param index 角色索引
 */
function editRole(id,  index) {
	$("#role_id").val(id);
	$.ajax({
		url : basePath + '/sys/role/getRoleById',
		method : 'post', 
		data : {id : id},
		dataType : "json",
		success : function(role) {
			$("#rolename").val(role.name);
			$("#roleordernum").numberbox('setValue', role.ordernum);
			$("#roleinfo").val(role.info);
			$("#roleid").val(role.id);
			$("#rolename").validatebox('isValid');
		}
	});
	$("#editDialog").dialog('open');
	initUserList(id);
	initMenuTree(id);
}

/**
 * 批量移除角色下的用户
 */
function removeRoleUsers(userId) {
	var ids;
	var num = 0;
	var roleId = $("#role_id").val();
	if(userId == null || userId.length <= 0) {
		var selects = $('#userList').datagrid('getSelections');
		if(selects.length <= 0) {
			alert("请先选择要删除的角色");
			return ;
		} else {
			var ids = "";
			for(var i=0;i<selects.length;i++) {
				ids = ids + selects[i].id +',';
				num ++;
			}
			ids = ids.substring(0, ids.length-1);
		}
	} else {
		ids = userId;
		num = 1;
	}
	var url = basePath + '/sys/userrole/delete';
	var args = {
			time : new Date(), 
			roleId : roleId,
			ids : ids,
			num : num
	}
	$.ajax({
		url : url, 
		data : args,
		method : 'post',
		success : function(data) {
			data = eval("("+data+")");
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
	    	reloadUserList();
		}
	});
}

/**
 * 单条移除角色下的用户
 */
function removeRoleUser(id, index) {
	removeRoleUsers(id);
}

/**
 * 获取所有选择的菜单节点
 */
function commitUpdateRoleMenu() {
	var roleId = $("#role_id").val();
	var ids = getMenuTreeSelects();
	var url = basePath+'/sys/rolemenu/update';
	var args = {
			time : new Date(),
			ids : ids,
			roleId : roleId
	};
	$.ajax({
		url : url, 
		data : args,
		method : 'post',
		dataType : "json",
		success : function(data) {
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
			$("#menuTree").tree('reload');
		}
	});
}

/**
 * 重载列表
 * 并恢复初始状态
 */
function reloadRoleList(key) {
	if(null == key) {
		key = '';
	}
	$('#RoleList').datagrid({
		queryParams : {name : key}
	});
	$('#RoleList').datagrid('reload');
	
	initState();
}

/**
 * 刷新用户列表
 */
function reloadUserList() {
	$('#userList').datagrid('reload');
}

/**
 * 初始状态：
 * 提交、取消按钮不可用
 * 取消所有选中的行
 * 隐藏多选列
 */
function initState() {
	$('#submit').linkbutton('disable');
	$('#cancel').linkbutton('disable');
	$('#RoleList').datagrid('clearSelections');
	$('#RoleList').datagrid('hideColumn', 'ck');
//	$('#RoleList').datagrid({ singleSelect: true });
}

/**
 * 编辑状态：
 * 显示多选列
 * 提交、取消按钮可用
 */
function activState() {
	$('#submit').linkbutton('enable');
	$('#cancel').linkbutton('enable');
	$('#RoleList').datagrid('showColumn', 'ck');
}