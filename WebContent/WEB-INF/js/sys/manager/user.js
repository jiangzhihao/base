
$(function() {
	
	/**
	 * 全局变量，记录搜索的参数（用户名）
	 */
	var global_userName;
	
	/**
	 * 全局变量，记录搜索的参数（登录名）
	 */
	var global_loginName;
	
	initUserList();
	
	initBtnClick();
	
})

/**
 * 初始化按钮组件事件
 */
function initBtnClick() {
	$("#cencelEdit").click(function() {
		$("#UserForm").form("clear");
		$("#editDialog").dialog("close");
	});
	$("#cencelAdd").click(function() {
		$("#UserForm2").form("clear");
		$("#addDialog").dialog("close");
	});
	$("#commitEditRoleBtn").click(function() {
		updateUser();
	});
	$("#commitAddRoleBtn").click(function() {
		addUser();
	});
	$("#search").click(function() {
		searchData();
	});
	$("#reset").click(function() {
		reset();
	});
}

/**
 * 初始化用户列表
 */
function initUserList() {
	$("#UserList").datagrid({
		url: basePath + '/sys/user/list',  
	    method : 'post',
	    rownumbers : true,
	    singleSelect : false,
	    columns:[[   
	        {field:'ck',align : 'center',checkbox:true, hidden:true}, 
	        {field:'userName',title:'用户名',width:'16%',align:'center'},   
	        {field:'userName',title:'登录帐号',width:'22%',align:'center'},  
	        {field:'telephone',title:'联系方式',width:'16%',align:'center', formatter : function(value, obj,index) {
	        	return value;
	        }},
	        {field:'mail',title:'邮箱',width:'20%',align:'center'},
	        {field:'deptNames',title:'部门',width:'12%',align:'center'},
	        {field:'id',title:'操作',width:'18%',align:'center', formatter : function(value, obj, index) {
	        	return  '<div style="cursor:pointer;width:50px;margin:0px auto"' 
				+ 'onclick="editUser('+obj.id+','+index+')"><img src='+basePath+'/images/app/btn_edit.png'
				+ ' style="vertical-align: middle;" title="编辑" /><span class="">编辑</span></div>';
	        }}   
	    ]]  ,
	    pagination : true,
	    pageSize : 15,
	    pageList : [10, 15, 20 ,25],
	    toolbar : [
			   		{
			   			text : '添加',
			   			iconCls : 'icon-add',
			   			handler : function() {
			   				$("#addDialog").dialog('open');
			   				$("#add_deptNames").combobox({
			   					url: basePath + '/sys/dept/list',   
			   				    valueField:'id',    
			   				    textField:'deptName',
			   				    multiple:true,
			   				    selectOnNavigation: ',',
			   				    editable : false
			   				});
			   			}
			   		},
			   		{
			   			text : '删除',
			   			iconCls : 'icon-remove',
			   			handler : function() {
			   				activState();
			   			}
			   		},
			   		{
			   			text : '提交',
			   			iconCls : 'icon-save',
			   			disabled : true,
			   			id:"submit",
			   			handler : function() {
			   				delUsers();
			   			}
			   		},
			   		{
			   			text : '取消',
			   			iconCls : 'icon-cancel',
			   			disabled : true,
			   			id:"cancel",
			   			handler : function() {
			   				initState();
			   			}
			   		}
				],
		onLoadSuccess : function() { //成功加载后的事件
		}
	});
}

/**
 * 初始化角色列表
 * （指定用户没有的角色）
 */
function initRoleList2(userId) {
	var url = basePath + "/sys/role/getRolesNotinUser";
	var args = {
			userId : userId, 
			time : new Date()
	};
	$("#RoleList2").datagrid({
		url: url,  
	    method : 'post',
	    rownumbers : true,
	    singleSelect : false,
	    queryParams: args,
	    columns:[[   
	        {field:'ck',align : 'center',checkbox:true}, 
	        {field:'name',title:'角色名',width:'35%',align:'center'},   
	        {field:'info',title:'说明',width:'45%',align:'center'},  
	    ]]  ,
	    pagination : true,
	    pageSize : 10,
	    pageList : [5, 10, 15 ],
	    toolbar : [
			   		{
			   			text : '添加',
			   			iconCls : 'icon-add',
			   			handler : function() {
			   				addRoles($("#user_id").val());
			   			}
			   		}
				],
	});
}

/**
 * 编辑用户（打开对话框）
 * @param id
 * @param index
 */
function editUser(id,index) {
	$("#user_id").val(id);
	initRoleList(id);
	initUserForm(id);
	$("#editDialog").dialog('open');
}

function initUserForm(id) {
	$.ajax({
		url : basePath + '/sys/user/getUserById',
		method : 'post', 
		data : {id : id},
		dataType : "json",
		success : function(user) {
			$("#edit_username").val(user.userName);
			$("#edit_username").validatebox('isValid');
			$("#edit_loginname").val(user.loginName);
			$("#edit_loginname").validatebox('isValid');
			$("#edit_gender").combobox('setValue',user.gender);
			$("#edit_mobile").val(user.mobile);
			$("#edit_mobile").validatebox('isValid');
			$("#edit_telephone").val(user.telephone);
			$("#edit_telephone").validatebox('isValid');
			$("#edit_mail").val(user.mail);
			$("#edit_mail").validatebox('isValid');
			var birthday = user.birthday.split("-");
			if(birthday.length < 3) {
				$("#edit_birthday").datebox('setValue',  new Date());
			} else {
				$("#edit_birthday").datebox('setValue',  birthday[0] + '/' + birthday[1] + '/' + birthday[2]);
			}
			$("#user_id").val(user.id);
			$("#edit_address").val(user.address);
			$("#edit_address").validatebox('isValid');
		}
	});
}

function addRoles(userId) {
	var selects = $("#RoleList2").datagrid('getSelections');
	if(null == selects || selects.length <= 0) {
		alert("请选择要添加的角色");
		return;
	}
	var ids = '';
	var num = 0;
	for(var i=0;i<selects.length;i++) {
		ids = ids + selects[i].id + ',';
		num ++;
	}
	ids = ids.substring(0, ids.length-1);
	var url = basePath + '/sys/userrole/addByUser';
	var args = {
			time : new Date(),
			ids : ids,
			userId : userId,
			num : num
	};
	$.ajax({
		url : url, 
		data : args ,
		dataType : 'json',
		method : 'post',
		success : function(data) {
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
			$("#RoleList2").datagrid('unselectAll');
			$("#RoleList2").datagrid('reload');
			$("#addRoleDialog").dialog('close');
			$("#RoleList").datagrid('reload');
		}
	});
}

/**
 * 批量删除用户
 */
function delUsers() {
	var selects = $("#UserList").datagrid('getSelections');
	if(null == selects || selects.length <= 0) {
		alert("请选择要删除的用户");
		return;
	}
	var ids = '';
	var num = 0;
	for(var i=0;i<selects.length;i++) {
		ids = ids + selects[i].id + ',';
		num ++;
	}
	ids = ids.substring(0, ids.length-1);
	var url = basePath+"/sys/user/del";
	var args = {
			time : new Date(),
			ids : ids,
			num : num
	}
	$.ajax({
		url : url, 
		data : args,
		method : 'post',
		dataType : 'json',
		success : function(data) {
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
			reloadUserList();
			initState();
		}
	});
}

/**
 * 删除用户下的角色
 * @param roleId
 * @param index
 */
function delRole(roleId, index) {
	var userId = $("#user_id").val();
	var url = basePath + '/sys/userrole/deleteRolesByUser';
	var args = {
			time : new Date(),
			userId : userId,
			ids : roleId,
			num : 1
	};
	$.ajax({
		url : url,
		data : args,
		method : 'post',
		dataType : 'json',
		success : function(data) {
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
			reloadRoleList();
		}
	});
}

/**
 * 批量删除用户下的角色
 */
function deleteRoles() {
	var userId = $("#user_id").val();
	var selects = $("#RoleList").datagrid('getSelections');
	if(null == selects || selects.length <= 0) {
		alert("请选择要删除的角色");
		return;
	}
	var ids = '';
	var num = 0;
	for(var i=0;i<selects.length;i++) {
		ids = ids + selects[i].id + ',';
		num ++;
	}
	ids = ids.substring(0, ids.length-1);
	var url = basePath+"/sys/userrole/deleteRolesByUser";
	var args = {
			url : url,
			ids : ids,
			userId : userId,
			num : num
	};
	$.ajax({
		url : url,
		data : args,
		dataType : 'json',
		method : 'post',
		success : function(data) {
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
			reloadRoleList();
		}
	});
}

/**
 * 初始化角色列表
 */
function initRoleList(userId) {
	$("#RoleList").datagrid({
		url: basePath + '/sys/userrole/getRolesByUser/'+userId,  
	    method : 'post',
	    rownumbers : true,
	    singleSelect : false,
	    columns:[[   
	        {field:'ck',align : 'center',checkbox:true}, 
	        {field:'name',title:'角色名',width:'22%',align:'center'},  
	        {field:'info',title:'角色名',width:'35%',align:'center'},  
	        {field:'id',title:'操作',width:'30%',align:'center', formatter : function(value, obj, index) {
	        	return  '<div style="cursor:pointer;width:50px;margin:0px auto"' 
				+ 'onclick="delRole('+obj.id+','+index+')"><img src='+basePath+'/images/app/btn_edit.png'
				+ ' style="vertical-align: middle;" title="移除" /><span class="">移除</span></div>';
	        }}   
	    ]]  ,
	    pagination : true,
	    pageSize : 15,
	    pageList : [10, 15, 20 ,25],
	    toolbar : [
			   		{
			   			text : '添加',
			   			iconCls : 'icon-add',
			   			handler : function() {
			   				var userId = $("#user_id").val();
			   				initRoleList2(userId);
			   				$("#addRoleDialog").dialog('open');
			   			}
			   		},
			   		{
			   			text : '移除',
			   			iconCls : 'icon-remove',
			   			handler : function() {
			   				deleteRoles();
			   			}
			   		}
				],
		onLoadSuccess : function() { //成功加载后的事件
		}
	});
}



/**
 * 更新用户基本信息
 */
function updateUser() {
	var userId = $("#user_id").val();
	$('#UserForm').form('submit', {    
	    url : basePath + "/sys/user/updateUser",    
	    onSubmit: function(){    
	    },    
	    success:function(data){
	    	data = eval("("+data+")");
	    	$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
	    	$('#UserForm').form('clear');
	    	initUserForm(userId);
	    }    
	});  
}

/**
 * 添加用户
 */
function addUser() {
	var userName = $("#add_username").val();
	var loginName = $("#add_loginname").val();
	var password = $("#add_password").val();
	var gender = $("#add_gender").combobox('getValue');
	var mobile = $("#add_mobile").val();
	var telephone = $("#add_telephone").val();
	var mail = $("#add_mail").val();
	var birthday = $("#add_birthday").datebox('getValue');
	var deptNames = $("#add_deptNames").combobox("getValues");
	var address = $("#add_address").val();
	var deptIds = "";
	for(var i=0;i<deptNames.length;i++) {
		deptIds = deptIds + deptNames[i] + ",";
	}
	var url = basePath + "/sys/user/addUser";
	var args = {
			userName : userName,
			loginName : loginName,
			password : password,
			gender : gender,
			mobile : mobile,
			telephone : telephone,
			mail : mail,
			birthday : birthday,
			deptNames : deptIds.substring(0, deptIds.length-1),
			address : address,
			time : new Date()
	}
	$.ajax({
		url : url, 
		data : args, 
		dataType : 'json',
		method : 'post',
		success : function(data) {
	    	$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
	    	$('#UserForm2').form('clear');
	    	$('#addDialog').dialog('close');
	    	reloadUserList();
		}
	});
}

/**
 * 搜索重置按钮
 */
function reset() {
	$("#user_username").textbox('setValue','');
	$("#user_username").textbox('setText','');
	
	$("#user_loginname").textbox('setValue','');
	$("#user_loginname").textbox('setText','');
	
	reloadUserList();
}

/**
 * 搜索用户
 */
function searchData() {
	var userName = $("#user_username").textbox("getValue");
	var loginName = $("#user_loginname").textbox("getValue");
	global_userName = userName;
	global_loginName = loginName;
	$('#UserList').datagrid({
		queryParams : {
			userName : userName,
			loginName : loginName
		}
	});
	$("#UserList").datagrid('reload');
}




/**
 * 重新加载用户列表，会清空请求参数
 */
function reloadUserList() {
	$('#UserList').datagrid({
		queryParams : {
			time : new Date()
		}
	});
	$("#UserList").datagrid('reload');
}

/**
 * 重新加载角色列表
 */
function reloadRoleList() {
	$("#RoleList").datagrid('reload');
}

/**
 * 初始状态：
 * 提交、取消按钮不可用
 * 取消所有选中的行
 * 隐藏多选列
 */
function initState() {
	$('#submit').linkbutton('disable');
	$('#cancel').linkbutton('disable');
	$('#UserList').datagrid('clearSelections');
	$('#UserList').datagrid('hideColumn', 'ck');
}

/**
 * 编辑状态：
 * 显示多选列
 * 提交、取消按钮可用
 */
function activState() {
	$('#submit').linkbutton('enable');
	$('#cancel').linkbutton('enable');
	$('#UserList').datagrid('showColumn', 'ck');
}
