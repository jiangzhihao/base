

$(function() {
	
	initMenuTree();
	
	initRoleList();
	
	initTreeClick();
	
	disabledForm();
	
	initBtnClick();
})

/**
 * 初始化按钮事件
 */
function initBtnClick() {
	$("#editMenu").click(function() {
		unDisabledForm();
		$('#saveCommit').linkbutton('enable');
		$('#cencel').linkbutton('enable');
	});
	$("#cencel").click(function() {
		$.post(basePath+"/sys/menu/getMenuById/"+$("#menu_id").val(), {time:new Date()}, function (data) {
			data = eval("("+data+")");
			$("#MenuForm").form('load',data);
			showBtn(data.isLeaf);
			disabledForm();
			$('#saveCommit').linkbutton('disable');
			$('#cencel').linkbutton('disable');
		});
		
	});
	$("#deleteMenu").click(function() {
		deleteMenu();
	});
	$("#saveCommit").click(function() {
		saveOrUpdateMenu();
	});
	$("#addInLevelMenu").click(function() {
		addMenu(true);
		$('#saveCommit').linkbutton('enable');
		$('#cencel').linkbutton('enable');
	});
	$("#addChildLevelMenu").click(function() {
		addMenu(false);
		$('#saveCommit').linkbutton('enable');
		$('#cencel').linkbutton('enable');
	});
}

/**
 * 添加同级菜单
 */
function addMenu(isInLevel) {
	var pid = $("#menu_id").val();
	var node = $("#menuTree").tree("getSelected");
	if(pid != node.id) {
		alert("数据错误，请刷新界面后重试");
		return;
	}
	$("#MenuForm").form('clear');
	$("#MenuForm").form('reset');
	unDisabledForm();
	if(isInLevel) {
		$.post(basePath+"/sys/menu/getMenuById/"+node.id, {time:new Date()}, function (data) {
			data = eval("("+data+")");
			$("#menuPid").val(data.pid);
			$("#menuIsLeaf").val(data.isLeaf);
			$("#menuLevel").val(data.level);
		});
	} else {
		$("#menuPid").val(pid);
		$("#menuIsLeaf").val(1);
		$("#menuLevel").val(2);
	}
}

/**
 * 点击事件
 */
function initTreeClick() {
	$('#menuTree').tree({
		onClick: function(node){
			var menuId = node.id;
			$("#menu_id").val(menuId);
			var isLeaf = $('#MenuTree').tree('isLeaf', menuId);
			if(isLeaf) {
				isLeaf = 1;
			} else {
				isLeaf = 0;
			}
			$('#RoleList').datagrid({
				queryParams : {
					menuId : menuId,
					isLeaf : isLeaf
				}
			});
			$('#RoleList').datagrid('reload');
			$.post(basePath+"/sys/menu/getMenuById/"+node.id, {time:new Date()}, function (data) {
				data = eval("("+data+")");
				$("#MenuForm").form('load',data);
				showBtn(data.isLeaf);
			});
			disabledForm();
			$('#add').linkbutton('enable');
		}
	});
}




/**
 * 初始化菜单树
 */
function initMenuTree() {
	$('#menuTree').tree({    
	    url: basePath + '/sys/menu/treeAll',   
	    onlyLeafCheck : false,
	    lines : true
	});  
}

/**
 * 初始化菜单列表
 * @param menuId
 */
function initRoleList(menuId) {
	var url = basePath + '/sys/role/getRolesByMenu';
	var args = {
			time : new Date()
	};
	$("#RoleList").datagrid({
		url: url,
	    method : 'post',
	    rownumbers : true,
	    singleSelect : false,
	    queryParams : args,
	    columns:[[   
	        {field:'ck',align : 'center',checkbox:true, hidden:true}, 
	        {field:'name',title:'角色名称',width:'28%',align:'center'},    
	        {field:'info',title:'简介',width:'50%',align:'center'},    
	        {field:'id',title:'操作',width:'18%',align:'center', formatter : function(value, obj,index) {
				return  '<div style="cursor:pointer;width:50px;margin:0px auto"' 
				+ 'onclick="removeRole('+obj.id+','+index+')"><img src='+basePath+'/images/app/btn_edit.png'
				+ ' style="vertical-align: middle;" title="移除" /><span class="">移除</span></div>';
	        }}    
	    ]]  ,
	    pagination : true,
	    pageSize : 5,
	    pageList : [5, 10, 15],
	    toolbar : [
			   		{
			   			text : '添加',
			   			iconCls : 'icon-add',
			   			disabled : true,
			   			id:"add",
			   			handler : function() {
			   				openAddRoleDialog();
			   			}
			   		},
			   		{
			   			text : '移除',
			   			iconCls : 'icon-remove',
			   			handler : function() {
			   				activState();
			   			}
			   		},
			   		{
			   			text : '提交',
			   			iconCls : 'icon-save',
			   			disabled : true,
			   			id:"submit",
			   			handler : function() {
			   				commitRemoveRoles();
			   			}
			   		},
			   		{
			   			text : '取消',
			   			iconCls : 'icon-cancel',
			   			disabled : true,
			   			id:"cancel",
			   			handler : function() {
			   				initState();
			   			}
			   		}
				],
		onLoadSuccess : function() { //成功加载后的事件
		}
	});
}

function saveOrUpdateMenu() {
	var url = basePath;
	var menuId = $("#menuId").val();
	if(null == menuId || menuId == '') {
		url = url + "/sys/menu/add";
	} else {
		url = url + "/sys/menu/update";
	}
	$('#MenuForm').form('submit', {
		url: url,
		onSubmit: function(){
		},
		success: function(data){
			data = eval("("+data+")");
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
			$("#menuTree").tree('reload');
  		    $("#MenuForm").form('clear');
  		    $("#MenuForm").form('reset');
  		    disabledForm();
  		    initBtnState();
  		    initState();
  		    $("#menu_id").val("");
  		    $("#roleList").datagrid("load", {});
  		  $('#add').linkbutton('disable');
		}
	});
}

/**
 * 单独移除一条角色
 * @param id 角色ID
 * @param index
 */
function removeRole(id, index) {
	var menuId = $("#menu_id").val();
	var ids = id;
	var url = basePath + "/sys/rolemenu/delete";
	var args = {
			time : new Date(), 
			ids : ids,
			num : 1,
			menuId : menuId
	};
	$.ajax({
		url : url,
		data : args,
		dataType : 'json',
		method : 'post',
		success : function(data) {
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
			$("#RoleList").datagrid("clearSelections");
			$("#RoleList").datagrid("reload");
			
		}
	});
}

/**
 * 批量移除角色
 */
function commitRemoveRoles() {
	var menuId = $("#menu_id").val();
	var selects = $('#RoleList').datagrid('getSelections');
	if(selects.length <= 0) {
		alert("请先选择要移除的角色");
		return ;
	} else {
		var ids = "";
		var num = 0;
		for(var i=0;i<selects.length;i++) {
			ids = ids + selects[i].id +',';
			num ++;
		}
		ids = ids.substring(0, ids.length-1);
		var url = basePath + "/sys/rolemenu/delete";
		var args = {
				time : new Date(), 
				ids : ids,
				num : num,
				menuId : menuId
		};
		$.ajax({
			url : url,
			data : args,
			dataType : 'json',
			method : 'post',
			success : function(data) {
				$.messager.show({
					title:'提示',
					msg:data.message,
					timeout:5000,//0为不消失
					width : 300,
					height : 100,
					showType:'slide',
				}); 
				$("#RoleList").datagrid("clearSelections");
				$("#RoleList").datagrid("reload");
				initState();
			}
		});
	}
}

/**
 * 根据ID删除菜单
 */
function deleteMenu() {
	var menuId = $("#menu_id").val();
	var node = $("#menuTree").tree("getSelected");
	if(node.id == menuId) {
		$.messager.confirm('确认',"确定要删除菜单 "+node.text+" 吗？",function(r){    
		    if (r){    
		          var url = basePath + "/sys/menu/delete";
		          var args = {
		        		  time : new Date(),
		        		  menuId : menuId
		          };
		          $.ajax({
		        	  url : url, 
		        	  data : args, 
		        	  dataType : 'json',
		        	  method : 'post',
		        	  success : function(data) {
		        		  $.messager.show({
		  					title:'提示',
		  					msg:data.message,
		  					timeout:5000,//0为不消失
		  					width : 300,
		  					height : 100,
		  					showType:'slide',
		  				  }); 
		        		  $("#menuTree").tree('reload');
		        		  $("#MenuForm").form('clear');
		        		  $("#MenuForm").form('reset');
		        		  disabledForm();
		        		  initBtnState();
		        		  initState();
		        		  $("#menu_id").val("");
		        		  $("#roleList").datagrid("load", {});
		        	  }
		          });
		    } else {
				return;
			}    
		}); 
	} 
}

/**
 * 根据选中的节点来显示不同的按钮
 * @param isLeaf
 */
function showBtn(isLeaf) {
	$('#editMenu').linkbutton('enable');
	$('#deleteMenu').linkbutton('enable');
	$('#saveCommit').linkbutton('disable');
	$('#cencel').linkbutton('disable');
	$('#addInLevelMenu').linkbutton('enable');
	
	if(!isLeaf) { //是叶子节点（则只能添加同级菜单）
		$('#addChildLevelMenu').linkbutton('enable');
	} else {
		$('#addChildLevelMenu').linkbutton('disable');
	}
}

/**
 * 展示添加角色对话框
 */
function openAddRoleDialog() {
	var menuId = $("#menuId").val();
	if(null == menuId || menuId.length < 0) {
		alert("数据错误，请重新选择要更新的菜单");
		return;
	}
	var url = basePath + "/sys/role/getRolesNotinMenu";
	var args = {
			menuId : menuId, 
			time : new Date()
	};
	$("#RoleList2").datagrid({
		url: url,
	    method : 'post',
	    rownumbers : true,
	    singleSelect : false,
	    queryParams : args,
	    columns:[[   
	  	        {field:'ck',align : 'center',checkbox:true}, 
	  	        {field:'name',title:'角色名',width:'35%',align:'center'},   
	  	        {field:'info',title:'说明',width:'45%',align:'center'},  
	  	    ]]  ,
  	    pagination : true,
  	    pageSize : 10,
  	    pageList : [5, 10, 15 ],
	    toolbar : [
			   		{
			   			text : '添加',
			   			iconCls : 'icon-add',
			   			handler : function() {
			   				addRoles(menuId);
			   			}
			   		},
		],
		onLoadSuccess : function() { //成功加载后的事件
		}
	});
	$("#addRoleDialog").dialog('open');
}

/**
 * 批量添加角色
 */
function addRoles(menuId) {
	var selects = $('#RoleList2').datagrid('getSelections');
	if(selects.length <= 0) {
		alert("请选择要添加的角色");
		return ;
	}
	var ids = "";
	var num = 0;
	for(var i=0;i<selects.length;i++) {
		ids = ids + selects[i].id +',';
		num ++;
	}
	ids = ids.substring(0, ids.length-1);
	var url = basePath + '/sys/rolemenu/addRoles';
	var args = {
			time : new Date(),
			ids : ids,
			num : num,
			menuId : menuId
	}
	$.ajax({
		url : url, 
		data : args, 
		dataType : 'json',
		method : 'post',
		success : function(data) {
			$.messager.show({
				title:'提示',
				msg:data.message,
				timeout:5000,//0为不消失
				width : 300,
				height : 100,
				showType:'slide',
			}); 
			$("#addRoleDialog").dialog('close');
			$("#RoleList").datagrid('reload');
		}
	});
}

/**
 * 初始化按钮状态
 */
function initBtnState() {
	$('#editMenu').linkbutton('disable');
	$('#deleteMenu').linkbutton('disable');
	$('#saveCommit').linkbutton('disable');
	$('#cencel').linkbutton('disable');
	$('#addInLevelMenu').linkbutton('disable');
	$('#addChildLevelMenu').linkbutton('disable');
}

/**
 * 使表单处于不可编辑状态
 */
function disabledForm() {
	$("#name").attr("readonly", true); 
	$("#orderNum").attr("readonly", true); 
	$("#info").attr("readonly", true); 
	$("#url").attr("readonly", true);
}

/**
 * 是表单处于可编辑状态
 */
function unDisabledForm() {
	$("#name").removeAttr("readonly");
	$("#orderNum").removeAttr("readonly");
	$("#info").removeAttr("readonly");
	$("#url").removeAttr("readonly");
}

/**
 * 初始状态：
 * 提交、取消按钮不可用
 * 取消所有选中的行
 * 隐藏多选列
 */
function initState() {
	$('#submit').linkbutton('disable');
	$('#cancel').linkbutton('disable');
	$('#add').linkbutton('disable');
	$('#RoleList').datagrid('clearSelections');
	$('#RoleList').datagrid('hideColumn', 'ck');
}

/**
 * 编辑状态：
 * 显示多选列
 * 提交、取消按钮可用
 */
function activState() {
	$('#submit').linkbutton('enable');
	$('#cancel').linkbutton('enable');
	$('#RoleList').datagrid('showColumn', 'ck');
}