package base.entity;

import java.io.Serializable;

/**
 * 
 * User
 *
 * @author 姜治昊
 * @time 2017年12月12日 下午9:51:29
 */
public class User implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String loginName;  
	
	private String userName;  
	
	private String password;  
	
	private String gender;  
	
	private String birthday;  
	
	private String telephone;  
	
	private String mobile;  
	
	private Integer linkman ;
	
	private String mail;  
	
	private String address;  
	
	private int isadmin;  
	
	private int isdelete;  
	
	private int ordernum;  
	
	private Department dept; 
	
	private String deptNames;
	
	public User() {
		super();
	}

	/**
	 * 
	 * @param id
	 * @param loginName  登录名
	 * @param userName   用户名
	 * @param password   密码
	 * @param gender     性别(0-女;1-男)
	 * @param birthday   出生日期
	 * @param telephone  电话
	 * @param mobile     手机
	 * @param linkman    账号责任人
	 * @param mail  邮箱
	 * @param address    地址
	 * @param isadmin    是否是管理员(0-否;1-是)
	 * @param isdelete   是否是删除(0-否;1-是)
	 * @param ordernum   排序
	 * @param dept  部门
	 */
	public User(Integer id, String loginName, String userName, String password,
			String gender, String birthday, String telephone, String mobile,
			Integer linkman, String mail, String address, int isadmin,
			int isdelete, int ordernum, Department dept) {
		super();
		this.id = id;
		this.loginName = loginName;
		this.userName = userName;
		this.password = password;
		this.gender = gender;
		this.birthday = birthday;
		this.telephone = telephone;
		this.mobile = mobile;
		this.linkman = linkman;
		this.mail = mail;
		this.address = address;
		this.isadmin = isadmin;
		this.isdelete = isdelete;
		this.ordernum = ordernum;
		this.dept = dept;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLoginName() {
		return loginName;
	}

	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getLinkman() {
		return linkman;
	}

	public void setLinkman(Integer linkman) {
		this.linkman = linkman;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getIsadmin() {
		return isadmin;
	}

	public void setIsadmin(int isadmin) {
		this.isadmin = isadmin;
	}

	public int getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(int isdelete) {
		this.isdelete = isdelete;
	}

	public int getOrdernum() {
		return ordernum;
	}

	public void setOrdernum(int ordernum) {
		this.ordernum = ordernum;
	}

	public Department getDept() {
		return dept;
	}

	public void setDept(Department dept) {
		this.dept = dept;
	}

	public String getDeptNames() {
		return deptNames;
	}

	public void setDeptNames(String deptNames) {
		this.deptNames = deptNames;
	}
	
	
	
}


