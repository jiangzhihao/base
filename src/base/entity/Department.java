package base.entity;

import java.io.Serializable;

/**
 * 
 * Department
 *
 * @author 姜治昊
 * @time 2017年12月12日 下午9:51:14
 */
public class Department implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;  
	
	private String deptName;  
	
	private String deptInfo;  
	
	private Integer pid;  
	
	private Integer isdelete;  
	
	private Integer ordernum;  
	
	private Integer type;

	public Department() {
		super();
	}

	/**
	 * 
	 * @param id
	 * @param deptName   部门名称
	 * @param deptInfo   描述
	 * @param pid   父id
	 * @param isdelete   删除(0-否;1-是)
	 * @param ordernum   排序
	 * @param type  类型
	 */
	public Department(Integer id, String deptName, String deptInfo, Integer pid,
			Integer isdelete, Integer ordernum, Integer type) {
		super();
		this.id = id;
		this.deptName = deptName;
		this.deptInfo = deptInfo;
		this.pid = pid;
		this.isdelete = isdelete;
		this.ordernum = ordernum;
		this.type = type;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getDeptInfo() {
		return deptInfo;
	}

	public void setDeptInfo(String deptInfo) {
		this.deptInfo = deptInfo;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getIsdelete() {
		return isdelete;
	}

	public void setIsdelete(Integer isdelete) {
		this.isdelete = isdelete;
	}

	public Integer getOrdernum() {
		return ordernum;
	}

	public void setOrdernum(Integer ordernum) {
		this.ordernum = ordernum;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
