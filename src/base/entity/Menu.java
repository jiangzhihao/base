package base.entity;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * Menu
 *
 * @author 姜治昊
 * @time 2017年12月12日 下午9:51:19
 */
public class Menu implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
    private Integer id;
  
    private String name;
  
    private String url;
  
    private String icon;
  
    private Integer pid;
  
    private Integer orderNum;
  
    private Integer level;
  
    private Integer isLeaf;
    
    private String info;
    
    private List<Menu> childs;

	public List<Menu> getChilds() {
		return childs;
	}

	public void setChilds(List<Menu> childs) {
		this.childs = childs;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public Integer getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(Integer isLeaf) {
		this.isLeaf = isLeaf;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param url
	 * @param icon
	 * @param pid 父节点
	 * @param orderNum 排序
	 * @param level 菜单等级
	 * @param isLeaf 是否是叶子结点
	 */
	public Menu(Integer id, String name, String url, String icon, Integer pid,
			Integer orderNum, Integer level, Integer isLeaf) {
		super();
		this.id = id;
		this.name = name;
		this.url = url;
		this.icon = icon;
		this.pid = pid;
		this.orderNum = orderNum;
		this.level = level;
		this.isLeaf = isLeaf;
	}

	public Menu() {
	}
    
}
