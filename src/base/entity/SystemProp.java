package base.entity;

import java.io.Serializable;

/**
 * 
 * SystemProp
 *
 * @author 姜治昊
 * @time 2018年2月3日 下午3:07:51
 */
public class SystemProp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Integer id;
	
	private String systemName;
	
	private String footerMsg;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}

	public String getFooterMsg() {
		return footerMsg;
	}

	public void setFooterMsg(String footerMsg) {
		this.footerMsg = footerMsg;
	}

	public SystemProp(Integer id, String systemName, String footerMsg) {
		super();
		this.id = id;
		this.systemName = systemName;
		this.footerMsg = footerMsg;
	}

	public SystemProp() {
		super();
	}

	@Override
	public String toString() {
		return "SystemProp [id=" + id + ", systemName=" + systemName
				+ ", footerMsg=" + footerMsg + "]";
	}
	
}
