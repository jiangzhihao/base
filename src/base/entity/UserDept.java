package base.entity;

/**
 * 
 * UserDept
 *
 * @author 姜治昊
 * @time 2018年1月6日 下午3:15:54
 */
public class UserDept {

	private Integer id;
	
	private Integer userId;
	
	private Integer deptId;
	
	private String info;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getDeptId() {
		return deptId;
	}

	public void setDeptId(Integer deptId) {
		this.deptId = deptId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public UserDept() {
		super();
	}
	
}
