package base.entity;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * Log
 *
 * @author 姜治昊
 * @time 2017年12月12日 下午9:52:44
 */
public class Log implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer id;
	
	private String type;
	
	private String description;
	
	private Date time;
	
	private String method;
	
	private User user;
	
	private String ip;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public Log(Integer id, String type, String description, Date time,
			String method, User user, String ip) {
		super();
		this.id = id;
		this.type = type;
		this.description = description;
		this.time = time;
		this.method = method;
		this.user = user;
		this.ip = ip;
	}

	/**
	 * 
	 * @param type 日志类型
	 * @param description 日志描述
	 * @param time 发生时间
	 * @param method 调用方法
	 * @param user 动作用户
	 * @param ip 请求源
	 */
	public Log(String type, String description, Date time, String method,
			User user, String ip) {
		super();
		this.type = type;
		this.description = description;
		this.time = time;
		this.method = method;
		this.user = user;
		this.ip = ip;
	}

	public Log() {
		super();
	}

	@Override
	public String toString() {
		return "Log [id=" + id + ", type=" + type + ", description="
				+ description + ", time=" + time + ", method=" + method
				+ ", user=" + user + ", ip=" + ip + "]";
	}

	

}
