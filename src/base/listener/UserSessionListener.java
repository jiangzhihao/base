package base.listener;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.web.context.WebApplicationContext;

import base.entity.User;
import base.service.sys.UserService;

/**
 * 
 * UserSessionListener
 *
 * @author 姜治昊
 * @time 2018年2月3日 下午2:31:30
 */
public class UserSessionListener implements HttpSessionListener {

	private UserService userService;
	
	
	@Override
	public void sessionCreated(HttpSessionEvent event) {
		// TODO Auto-generated method stub
		WebApplicationContext context = (WebApplicationContext) event.getSession().getServletContext().getAttribute(WebApplicationContext.ROOT_WEB_APPLICATION_CONTEXT_ATTRIBUTE);
		userService = context.getBean(UserService.class);
	}

	/**
	 * Session过期之后自动清除用户缓存
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent event) {
		// TODO Auto-generated method stub
		Object obj = event.getSession().getAttribute("LOGIN_USER");
		if(null != obj) {
			User user = (User) obj;
			userService.clearCache(user.getId());
		}
	}

}
