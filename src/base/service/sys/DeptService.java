package base.service.sys;

import java.util.List;

import base.entity.Department;

/**
 * 
 * DeptService
 *
 * @author 姜治昊
 * @time 2018年1月17日 下午5:07:02
 */
public interface DeptService {

	/**
	 * 获取部门列表
	 * @return
	 */
	List<Department> getDeptList();
	
}
