package base.service.sys.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import base.dao.sys.UserRoleDao;
import base.entity.Role;
import base.entity.User;
import base.service.sys.UserRoleService;
import base.util.EasyUiUtil;
import base.util.ResultMessage;
import base.util.page.OraclePageHelper;
import base.util.page.PageHelper;

/**
 * 
 * UserRoleServiceImpl
 *
 * @author 姜治昊
 * @time 2018年1月4日 下午2:05:20
 */
@Service
public class UserRoleServiceImpl implements UserRoleService {

	@Autowired
	private UserRoleDao userRoleDao;
	
	/**
	 * 
	 */
	@Override
	public String getUserByRoleId(Integer id, int page, int rows) {
		// TODO Auto-generated method stub
		PageHelper pageHelper = (new OraclePageHelper()).getPageParam(page, rows);
		List<User> users =  userRoleDao.getUserByRoleId(id, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int total = userRoleDao.getUserByRoleIdCount(id);
		return EasyUiUtil.dataGridFormat(total, users);
	}

	/**
	 * 
	 */
	@Override
	public ResultMessage<?> deleteUserRoleById(int num, String ids, Integer roleId) {
		// TODO Auto-generated method stub
		int result = userRoleDao.deleteUserRoleById(ids, roleId);
		if(result == num) {
			return ResultMessage.createSuccessResultMessage("成功移除"+num+"条记录");
		} else {
			return ResultMessage.createErrorResultMessage("移除失败");
		}
	}

	/**
	 * 为角色批量添加用户
	 */
	@Override
	public ResultMessage<?> addRoleUsers(Integer roleId, int num, String ids) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> params = new ArrayList<>();
		if(null != ids && ids.length() > 0) {
			String[] idsarray = ids.split(",");
			for(int i=0;i<idsarray.length;i++) {
				Map<String, Object> map = new HashMap<>(idsarray.length);
				map.put("userId", idsarray[i]);
				map.put("roleId", roleId);
				params.add(map);
			}
		}
		int result = userRoleDao.addRoleUsers(params);
		if(result == num) {
			return ResultMessage.createSuccessResultMessage("成功添加"+result+"条记录");
		} else {
			if(result > 0) {
				return ResultMessage.createSuccessResultMessage("添加了"+result+"条记录，未全部更新成功");
			} else {
				return ResultMessage.createErrorResultMessage("添加失败");
			}
		}
	}
	
	/**
	 * 为用户批量添加角色
	 */
	@Override
	public ResultMessage<?> addRoleUsersByUser(Integer userId, int num, String ids) {
		List<Map<String, Object>> params = new ArrayList<>();
		if(null != ids && ids.length() > 0) {
			String[] idsarray = ids.split(",");
			for(int i=0;i<idsarray.length;i++) {
				Map<String, Object> map = new HashMap<>(idsarray.length);
				map.put("roleId", idsarray[i]);
				map.put("userId", userId);
				params.add(map);
			}
		}
		int result = userRoleDao.addUserRoles(params);
		if(result == num) {
			return ResultMessage.createSuccessResultMessage("成功添加"+result+"条记录");
		} else {
			if(result > 0) {
				return ResultMessage.createSuccessResultMessage("添加了"+result+"条记录，未全部更新成功");
			} else {
				return ResultMessage.createErrorResultMessage("添加失败");
			}
		}
	}

	/**
	 * 根据用户获取角色
	 */
	@Override
	public String getRolesByUser(Integer userId, int page, int rows) {
		// TODO Auto-generated method stub
		PageHelper pageHelper = new OraclePageHelper();
		pageHelper.getPageParam(page, rows);
		List<Role> roles =  userRoleDao.getRolesByUser(userId, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int count = userRoleDao.getRolesByUserCount(userId);
		return EasyUiUtil.dataGridFormat(count, roles);
	}

	/**
	 * 删除用户下的角色
	 */
	@Override
	public ResultMessage<?> deleteRolesByUser(Integer userId, String ids, int num) {
		// TODO Auto-generated method stub
		int result = userRoleDao.deleteRolesByUser(userId, ids);
		if(result == num) {
			return ResultMessage.createSuccessResultMessage("成功删除"+result+"条记录");
		} else {
			return ResultMessage.createErrorResultMessage("删除失败，删除了"+result+"条记录");
		}
	}
	

}
