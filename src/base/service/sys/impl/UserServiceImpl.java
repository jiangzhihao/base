package base.service.sys.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

import base.dao.sys.UserDao;
import base.dao.sys.UserDeptDao;
import base.entity.User;
import base.service.sys.UserService;
import base.util.EasyUiUtil;
import base.util.ResultMessage;
import base.util.page.OraclePageHelper;
import base.util.page.PageHelper;

/**
 * 
 * UserServiceImpl
 *
 * @author 姜治昊
 * @time 2017年12月13日 下午9:35:27
 */
@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;
	
	@Autowired
	private UserDeptDao userDeptDao;
	
	/**
	 * 登录认证
	 */
	@Override
	public User authentication(String loginname, String password) {
		// TODO Auto-generated method stub
		return userDao.authentication(loginname, password);
	}
	
	/**
	 * 获取用户列表
	 * @return
	 */
	@Override
	public String getUserList(String userName, String loginName, int page, int rows) {
		if(null != userName && !"".equals(userName) && userName.length() > 0) {
			userName = "%" + userName + "%";
		}
		if(null != loginName && !"".equals(loginName) && loginName.length() > 0) {
			loginName = "%" + loginName + "%";
		}
		PageHelper pageHelper = new OraclePageHelper();
		pageHelper.getPageParam(page, rows);
		List<User> users = userDao.getUserList(userName, loginName, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int count = userDao.getUserListCount(userName, loginName);
		return EasyUiUtil.dataGridFormat(count, users);
	}

	/**
	 * 获取不在该角色下的用户
	 */
	@Override
	public String getUserNotInRoleById(Integer roleId, int page, int rows) {
		// TODO Auto-generated method stub
		PageHelper pageHelper = new OraclePageHelper();
		pageHelper.getPageParam(page, rows);
		List<User> users = userDao.getUserNotInRoleById(roleId, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int total = userDao.getUserNotInRoleByIdCount(roleId);
		return EasyUiUtil.dataGridFormat(total, users);
	}
	
	/**
	 * 批量删除用户
	 */
	@Override
	public ResultMessage<?> deleteUsers(String ids, int num) {
		int result = userDao.deleteUsers(ids);
		if(num == result) {
			return ResultMessage.createSuccessResultMessage("成功更新了"+result+"条数据");
		} else {
			return ResultMessage.createErrorResultMessage("更新失败，更新了"+result+"条数据");
		}
	}

	/**
	 * 根据ID获取用户
	 */
	@Override
	public User getUserById(Integer id) {
		// TODO Auto-generated method stub
		return userDao.getUserById(id);
	}
	
	/**
	 * 修改用户基本信息
	 */
	@Override
	public ResultMessage<?> updateUser(User user) {
		int result = userDao.updateUser(user);
		if(result == 1) {
			return ResultMessage.createSuccessResultMessage("更新成功");
		} else {
			return ResultMessage.createErrorResultMessage("更新失败");
		}
	}
	
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	@Override
	public ResultMessage<?> addUser(User user) {
		int resultUser = userDao.addUser(user);
		if(resultUser == 1) {
			String deptIdss = user.getDeptNames();
			String[] ids = deptIdss.split(",");
			List<Integer> deptIds = new ArrayList<>(ids.length);
			for(int i=0;i<ids.length;i++) {
				deptIds.add(Integer.parseInt(ids[i]));
			}
			int resultUserDept = userDeptDao.addUserDepts(deptIds, user.getId());
			if(resultUserDept == ids.length) {
				return ResultMessage.createSuccessResultMessage("添加成功");
			}
		}
		return ResultMessage.createErrorResultMessage("添加失败");
	}
	
	/**
	 * 清除当前用户的缓存
	 * @param id
	 */
	@Override
	@CacheEvict(value = {"userMenuTree"})
	public void clearCache(Integer id) {
		
	}

}
