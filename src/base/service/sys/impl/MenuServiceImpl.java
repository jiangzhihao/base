package base.service.sys.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import base.dao.sys.MenuDao;
import base.entity.Menu;
import base.service.sys.MenuService;
import base.util.ResultMessage;

/**
 * 
 * MenuServiceImpl
 *
 * @author 姜治昊
 * @time 2017年12月17日 下午2:12:34
 */
@Service
public class MenuServiceImpl implements MenuService {

	@Autowired
	private MenuDao menuDao;

	/**
	 * 获取用户的菜单
	 */
	@Override
	@Cacheable(value = { "userMenuTree" })
	public List<Menu> getUserMenus(Integer userId) {
		// TODO Auto-generated method stub
		List<Menu> menus = menuDao.getMenuByUser(userId);
		return menus;
	}

	/**
	 * 初始化目录树
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getMenuTree(Integer roleId) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> result = menuDao.getMenuTree(roleId);
		if(null != result && result.size() > 0) {
			for(int i=0;i<result.size();i++) {
				Map<String, Object> menuItem1 = result.get(i);
				menuItem1.put("state", "open");
				menuItem1.put("checked", false);
				List<Map<String, Object>> menuItem2 = (List<Map<String, Object>>) menuItem1.get("child");
				if(null != menuItem2 && menuItem2.size() > 0) {
					for(int j=0;j<menuItem2.size();j++) {
						Map<String, Object> menuitem = menuItem2.get(j);
						menuitem.put("state", "close");
						String checked = menuitem.get("checked").toString();
						if(null == checked || checked.length() <= 0 || "1".equals(checked)) {
							menuitem.put("checked", false);
						} else {
							menuitem.put("checked", true);
						}
					}
				}
			}
		}
		return result;
	}
	
	/**
	 * 初始化目录树
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getMenuTreeAll() {
		// TODO Auto-generated method stub
		List<Map<String, Object>> result = menuDao.getMenuTreeAll();
		if(null != result && result.size() > 0) {
			for(int i=0;i<result.size();i++) {
				Map<String, Object> menuItem1 = result.get(i);
				menuItem1.put("state", "open");
				List<Map<String, Object>> menuItem2 = (List<Map<String, Object>>) menuItem1.get("child");
				if(null != menuItem2 && menuItem2.size() > 0) {
					for(int j=0;j<menuItem2.size();j++) {
						Map<String, Object> menuitem = menuItem2.get(j);
						menuitem.put("state", "close");
					}
				}
			}
		}
		return result;
	}

	/**
	 * 根据ID获取菜单详细信息
	 */
	@Override
	public Menu getMenuById(Integer menuId) {
		// TODO Auto-generated method stub
		return menuDao.getMenuById(menuId);
	}

	/**
	 * 根据ID删除菜单
	 * @param id
	 * @return
	 */
	@Override
	public ResultMessage<?> delete(Integer id) {
		// TODO Auto-generated method stub
		int result = menuDao.delete(id);
		if(1 == result) {
			return ResultMessage.createSuccessResultMessage("成功删除"+result+"条记录");
		} else {
			return ResultMessage.createErrorResultMessage("删除失败");
		}
	}

	/**
	 * 根据ID更新菜单基本信息
	 * @param id
	 * @return
	 */
	@Override
	public ResultMessage<?> update(Menu menu) {
		// TODO Auto-generated method stub
		int result = menuDao.update(menu);
		if(1 == result) {
			return ResultMessage.createSuccessResultMessage("成功更新"+result+"条记录");
		} else {
			return ResultMessage.createErrorResultMessage("更新失败");
		}
	}

	/**
	 * 新增菜单
	 * @param menu
	 * @return
	 */
	@Override
	public ResultMessage<?> addMenu(Menu menu) {
		// TODO Auto-generated method stub
		if(null == menu.getIcon()) {
			menu.setIcon("default.png");
		}
		int result = menuDao.addMenu(menu);
		if(1 == result) {
			return ResultMessage.createSuccessResultMessage("成功添加"+result+"条记录");
		} else {
			return ResultMessage.createErrorResultMessage("添加失败");
		}
	}

}
