package base.service.sys.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import base.dao.sys.RoleDao;
import base.dao.sys.RoleMenuDao;
import base.dao.sys.UserRoleDao;
import base.entity.Role;
import base.service.sys.RoleService;
import base.util.EasyUiUtil;
import base.util.ResultMessage;
import base.util.page.OraclePageHelper;
import base.util.page.PageHelper;

/**
 * 
 * RoleServiceImpl
 *
 * @author 姜治昊
 * @time 2018年1月3日 下午5:26:23
 */
@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;
	
	@Autowired
	private RoleMenuDao roleMenuDao;
	
	@Autowired
	private UserRoleDao userRoleDao;
	
	/**
	 * 
	 */
	@Override
	public String getRoleList(String name, int page, int rows) {
		// TODO Auto-generated method stub
		PageHelper pageHelper = new OraclePageHelper();
		pageHelper.getPageParam(page, rows);
		List<Role> roles = roleDao.getRoleList(name, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int totle = roleDao.getRoleListCount(name);
		return EasyUiUtil.dataGridFormat(totle, roles);
	}
	
	/**
	 * 添加角色
	 */
	@Override
	public ResultMessage<?> addRole(Role role) {
		int result = roleDao.addRole(role);
		if(result == 1) {
			return ResultMessage.createSuccessResultMessage("添加成功");
		} else {
			return ResultMessage.createErrorResultMessage("添加失败"); 
		}
	}
	
	/**
	 * 删除角色
	 * @param num
	 * @param ids
	 * @return
	 */
	@Override
	public ResultMessage<?> deleteRoles(int num, String ids) {
		int resultOfDeleteForeignKeyToUser = userRoleDao.deleteUserRoleByRole(ids);
		System.out.println("清除了"+resultOfDeleteForeignKeyToUser+"条用户-角色关系");
		int resultOfDeleteForeignKeyToMenu = roleMenuDao.deleteRoleMenuByRole(ids);
		System.out.println("清除了"+resultOfDeleteForeignKeyToMenu+"条角色-菜单关系");
		int result = roleDao.deleteRoles(ids);
		if(result == num) {
			return ResultMessage.createSuccessResultMessage("成功删除了"+num+"条记录");
		} else {
			return ResultMessage.createSuccessResultMessage("删除失败");
		}
	}
	
	/**
	 * 根据ID获取
	 * @param id
	 * @return
	 */
	@Override	
	public Role getRoleById(Integer id) {
		return roleDao.getRoleById(id);
	}
	
	/**
	 * 更新
	 */
	@Override	
	public ResultMessage<?> updateRole(Role role) {
		int result = roleDao.updateRole(role);
		if(result == 1) {
			return ResultMessage.createSuccessResultMessage("更新成功");
		} else {
			return ResultMessage.createErrorResultMessage("更新失败"); 
		}
	}

	/**
	 * 用户没有的角色
	 */
	@Override
	public String getRolesNotinUser(Integer userId, int page, int rows) {
		// TODO Auto-generated method stub
		PageHelper pageHelper = new OraclePageHelper();
		pageHelper.getPageParam(page, rows);
		List<Role> roles = roleDao.getRolesNotinUser(userId, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int count = roleDao.getRolesNotinUserCount(userId);
		return EasyUiUtil.dataGridFormat(count, roles);
	}
	
	/**
	 * 菜单没有的角色
	 */
	@Override
	public String getRolesNotinMenu(Integer menuId, int page, int rows) {
		// TODO Auto-generated method stub
		PageHelper pageHelper = new OraclePageHelper();
		pageHelper.getPageParam(page, rows);
		List<Role> roles = roleDao.getRolesNotinMenu(menuId, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int count = roleDao.getRolesNotinMenuCount(menuId);
		return EasyUiUtil.dataGridFormat(count, roles);
	}

	/**
	 * 根据菜单获取角色
	 */
	@Override
	public String getRolesByMenu(int isLeaf, Integer menuId, int page, int rows) {
		// TODO Auto-generated method stub
		PageHelper pageHelper = new OraclePageHelper();
		pageHelper.getPageParam(page, rows);
		List<Role> roles = roleDao.getRolesByMenu(isLeaf, menuId, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int count = roleDao.getRolesByMenuCount(isLeaf, menuId);
		return EasyUiUtil.dataGridFormat(count, roles);
	}

}
