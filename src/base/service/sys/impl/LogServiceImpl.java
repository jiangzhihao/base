package base.service.sys.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import base.dao.sys.LogDao;
import base.entity.Log;
import base.service.sys.LogService;
import base.util.EasyUiUtil;
import base.util.page.OraclePageHelper;
import base.util.page.PageHelper;

/**
 * 
 * LogServiceImpl
 *
 * @author 姜治昊
 * @time 2018年2月1日 下午10:17:49
 */
@Service
public class LogServiceImpl implements LogService{

	@Autowired
	private LogDao logDao;
	
	/**
	 * 存储日志
	 */
	@Override
	public int saveLog(Log log) {
		// TODO Auto-generated method stub
		return logDao.saveLog(log);
	}

	/**
	 * 获取（条件查询）日志
	 */
	@Override
	public String getLogs(String userName, String type, Date startTime, Date endTime, int page, int rows) {
		// TODO Auto-generated method stub
		PageHelper pageHelper = new OraclePageHelper();
		pageHelper.getPageParam(page, rows);
		List<Log> logs = logDao.getLogs(userName, type, startTime, endTime, pageHelper.getPageParam1(), pageHelper.getPageParam2());
		int totle = logDao.getLogsCount(userName, type, startTime, endTime);
		return EasyUiUtil.dataGridFormat(totle, logs);
	}

}
