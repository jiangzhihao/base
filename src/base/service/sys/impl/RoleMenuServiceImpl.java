package base.service.sys.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import base.dao.sys.RoleMenuDao;
import base.service.sys.RoleMenuService;
import base.util.ResultMessage;

/**
 * 
 * RoleMenuServiceImpl
 *
 * @author 姜治昊
 * @time 2018年1月4日 下午10:23:01
 */
@Service
public class RoleMenuServiceImpl implements RoleMenuService {

	@Autowired
	private RoleMenuDao roleMenuDao;
	
	/**
	 * 更新角色对应的菜单
	 */
	@Override
	public ResultMessage<?> updateRoleMenu(String ids, Integer roleId) {
		// TODO Auto-generated method stub
		List<Map<String, Object>> addMenus = new ArrayList<>();
		StringBuilder delIds = new StringBuilder();
		List<Integer> myMenus = roleMenuDao.getMenuIdsByRole(roleId);
		List<String> menuIds = Arrays.asList(ids.split(","));
		if(null == myMenus || myMenus.size() <= 0) {
			if(null != menuIds && menuIds.size() > 0) {
				for(int z=0;z<menuIds.size();z++) {
					Map<String, Object> map = new HashMap<>(menuIds.size());
					map.put("roleId", roleId);
					map.put("menuId", Integer.parseInt(menuIds.get(z)));
					addMenus.add(map);
				}
			}
		} else {
			for(int i=0;i<menuIds.size();i++) {
				Integer menuId = Integer.parseInt(menuIds.get(i));
				if(myMenus.contains(menuId)) {
					myMenus.remove(menuId);
				} else {
					Map<String, Object> map = new HashMap<>(2);
					map.put("roleId", roleId);
					map.put("menuId", menuId);
					addMenus.add(map);
				}
			}
			for(int j=0;j<myMenus.size();j++) {
				delIds.append(myMenus.get(j) + ",");
			}
		}
		if(addMenus.size() > 0) {
			int resultAdd = roleMenuDao.addRoleMenu(addMenus);
			System.out.println("增加了"+resultAdd+"条菜单权限");
		}
		if(delIds.length() > 0) {
			String delidsStr = delIds.toString();
			int resultDel = roleMenuDao.deleteRoleMenu(roleId, delidsStr.substring(0, delidsStr.length()-1));
			System.out.println("删除了"+resultDel+"条菜单权限");
		}
		return ResultMessage.createSuccessResultMessage("更新成功");
	}

	/**
	 * 删除关联关系
	 */
	@Override
	public ResultMessage<?> deleteMenuRoles(int num, Integer menuId, String ids) {
		// TODO Auto-generated method stub
		int result = roleMenuDao.delete(menuId, ids);
		if(num == result) {
			return ResultMessage.createSuccessResultMessage("成功移除"+result+"条记录");
		} else {
			return ResultMessage.createErrorResultMessage("移除失败，移除了"+result+"条记录");
		}
	}

	/**
	 * 批量添加角色
	 */
	@Override
	public ResultMessage<?> addRoles(String ids, Integer menuId, int num) {
		// TODO Auto-generated method stub
		List<Integer> intIds = new ArrayList<>();
		String[] idsArr = ids.split(",");
		for(int i=0;i<idsArr.length;i++) {
			String id = idsArr[i];
			if(null != id) {
				intIds.add(Integer.parseInt(id));
			}
		}
		int result = roleMenuDao.addRoles(intIds, menuId);
		if(result == num) {
			return ResultMessage.createSuccessResultMessage("成功添加了"+result+"条记录");
		} else {
			return ResultMessage.createErrorResultMessage("添加失败，更新了"+result+"条记录");		
		}
	}

}
