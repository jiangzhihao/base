package base.service.sys.impl;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import base.dao.sys.SystemDao;
import base.entity.SystemProp;
import base.service.sys.SystemService;

/**
 * 
 * SystemServiceImpl
 *
 * @author 姜治昊
 * @time 2018年2月3日 下午3:12:47
 */
@Service
public class SystemServiceImpl implements SystemService {

	private CacheManager cacheManager;
	
	@Autowired
	private SystemDao systemDao;
	
	/**
	 * 初始化缓存数据（系统的基本属性信息）
	 * @return
	 */
	@PostConstruct
	public void initSystem() {
		Cache cache = cacheManager.getCache("system");
		SystemProp prop = systemDao.initSystem();
		cache.put("SYSTEM_PROPS", prop);
	}
	
	@Autowired
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
	
	@Override
	@Cacheable(value="system", key="#key")
	public SystemProp getSystemProps(String key) {
		return null;
	}

}
