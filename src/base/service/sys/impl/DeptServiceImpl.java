package base.service.sys.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import base.dao.sys.DepartmentDao;
import base.entity.Department;
import base.service.sys.DeptService;

/**
 * 
 * DeptServiceImpl
 *
 * @author 姜治昊
 * @time 2018年1月17日 下午5:07:39
 */
@Service
public class DeptServiceImpl implements DeptService {

	@Autowired
	private DepartmentDao deptDao; 
	
	/**
	 * 获取部门列表
	 */
	@Override
	public List<Department> getDeptList() {
		// TODO Auto-generated method stub
		return deptDao.getDeptList();
	}

}
