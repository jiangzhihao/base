package base.service.sys;

import base.entity.SystemProp;

/**
 * 系统配置属性信息
 * SystemService
 *
 * @author 姜治昊
 * @time 2018年2月3日 下午3:11:10
 */
public interface SystemService {

	/**
	 * 获取系统初始化参数
	 * @param key
	 * @return
	 */
	SystemProp getSystemProps(String key);
	
}
