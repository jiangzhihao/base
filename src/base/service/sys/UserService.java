package base.service.sys;

import base.entity.User;
import base.util.ResultMessage;

/**
 * 
 * UserService
 *
 * @author 姜治昊
 * @time 2017年12月13日 下午9:32:48
 */
public interface UserService {

	/**
	 * 登录认证
	 * @param loginname
	 * @param password
	 * @return
	 */
	User authentication(String loginname, String password);
	
	/**
	 * 获取用户列表
	 * @param userName
	 * @param loginName
	 * @param page
	 * @param rows
	 * @return
	 */
	String getUserList(String userName, String loginName, int page, int rows);
	
	/**
	 * 获取不属于某角色下的用户列表
	 * @param roleId
	 * @param page
	 * @param rows
	 * @return
	 */
	String getUserNotInRoleById(Integer roleId, int page, int rows);
	
	/**
	 * 批量删除用户
	 * @param ids
	 * @param num
	 * @return
	 */
	ResultMessage<?> deleteUsers(String ids, int num);
	
	/**
	 * 根据ID 获取用户
	 * @param id
	 * @return
	 */
	User getUserById(Integer id);
	
	/**
	 * 更新用户基本信息
	 * @param user
	 * @return
	 */
	ResultMessage<?> updateUser(User user);
	
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	ResultMessage<?> addUser(User user);
	
	/**
	 * 清除缓存
	 * @param id
	 */
	void clearCache(Integer id);
}
