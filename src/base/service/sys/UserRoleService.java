package base.service.sys;

import org.springframework.stereotype.Service;

import base.util.ResultMessage;

/**
 * 
 * UserRoleService
 *
 * @author 姜治昊
 * @time 2018年1月4日 下午2:04:55
 */
@Service
public interface UserRoleService {

	/**
	 * 根据角色获取用户
	 * @param id
	 * @param page
	 * @param rows
	 * @return
	 */
	String getUserByRoleId(Integer id, int page, int rows);
	
	/**
	 * 批量移除用户角色关系
	 * @param num 修改的数量
	 * @param ids 用户ID
	 * @param roleId 角色ID
	 * @return
	 */
	ResultMessage<?>  deleteUserRoleById(int num, String ids, Integer roleId);
	
	/**
	 * 批量添加角色用户
	 * @param roleId
	 * @param num 应该修改的记录数目
	 * @param ids
	 * @return
	 */
	ResultMessage<?> addRoleUsers(Integer roleId, int num, String ids);
	
	/**
	 * 为用户批量添加角色
	 * @param userId
	 * @param num
	 * @param ids
	 * @return
	 */
	ResultMessage<?> addRoleUsersByUser(Integer userId, int num, String ids);
	
	/**
	 * 根据用户获取对应的角色
	 * @param userId
	 * @param page
	 * @param rows
	 * @return
	 */
	String getRolesByUser(Integer userId, int page, int rows);
	
	/**
	 * 删除用户下的角色
	 * @param userId
	 * @param ids
	 * @param num 应该修改的记录数目
	 * @return
	 */
	ResultMessage<?> deleteRolesByUser(Integer userId, String ids, int num);
}
