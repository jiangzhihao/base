package base.service.sys;

import base.entity.Role;
import base.util.ResultMessage;

/**
 * 
 * RoleService
 *
 * @author 姜治昊
 * @time 2018年1月3日 下午5:25:53
 */
public interface RoleService {

	/**
	 * 获取角色列表（含有条件）
	 * @param name 角色，名称
	 * @param page
	 * @param rows
	 * @return
	 */
	String getRoleList(String name, int page, int rows);
	
	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
	ResultMessage<?> addRole(Role role);
	
	/**
	 * 修改角色
	 * @param role
	 * @return
	 */
	ResultMessage<?> updateRole(Role role);
	
	/**
	 * 删除角色
	 * @param num
	 * @param ids
	 * @return
	 */
	ResultMessage<?> deleteRoles(int num, String ids);
	
	/**
	 * 根据ID获取角色对象
	 * @param id
	 * @return
	 */
	Role getRoleById(Integer id);
	
	/**
	 * 获取不属于指定用户的角色
	 * @param userId
	 * @param page
	 * @param rows
	 * @return
	 */
	String getRolesNotinUser(Integer userId, int page, int rows);
	
	/**
	 * 获取没有与指定菜单关联的所有角色
	 * @param menuId
	 * @param page
	 * @param rows
	 * @return
	 */
	String getRolesNotinMenu(Integer menuId, int page, int rows);
	
	/**
	 * 根据菜单获取角色
	 * @param isLeaf
	 * @param menuId
	 * @param page
	 * @param rows
	 * @return
	 */
	String getRolesByMenu(int isLeaf, Integer menuId,  int page, int rows);
	
	
}
