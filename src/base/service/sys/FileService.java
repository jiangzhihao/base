package base.service.sys;

import java.util.List;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import base.util.ResultMessage;

/**
 * 文件上传下载相关
 * @author JZH
 *
 */
public interface FileService {

	/**
	 * 上传文件
	 * @param description
	 * @param file
	 * @return 
	 */
	ResultMessage<?> uploadFile(String description, MultipartFile file, Integer userId);
	
	/**
	 * 获取所有上传的文件
	 * @return
	 */
	List<Map<String, Object>> allFiles();
	
	/**
	 * 文件下载
	 * @param id
	 * @return
	 */
	ResponseEntity<byte[]> downloadFile(Integer id);
	
	/**
	 * 通过ID获取文件信息
	 * @param id
	 * @return
	 */
	Map<String, Object> getPathAndNameById(Integer id);
	
	
}
