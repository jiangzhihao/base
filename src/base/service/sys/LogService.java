package base.service.sys;

import java.util.Date;

import base.entity.Log;

/**
 * 
 * LogService
 *
 * @author 姜治昊
 * @time 2018年2月1日 下午10:02:40
 */
public interface LogService {
	
	/**
	 * 保存日志记录
	 * @param log
	 * @return
	 */
	int saveLog(Log log);
	
	/**
	 * 获取（条件查询）日志
	 * @param userName
	 * @param type
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param rows
	 * @return
	 */
	String getLogs(String userName, String type, Date startTime, Date endTime, int page, int rows);

}
