package base.service.sys;

import java.util.List;
import java.util.Map;

import base.entity.Menu;
import base.util.ResultMessage;

/**
 * 
 * MenuService
 *
 * @author 姜治昊
 * @time 2017年12月17日 下午2:11:52
 */
public interface MenuService {

	/**
	 * 根据ID获取菜单详细信息
	 * @param menuId
	 * @return
	 */
	Menu getMenuById(Integer menuId);
	
	/**
	 * 获取用户的菜单
	 * @param userId
	 * @return
	 */
	List<Menu> getUserMenus(Integer userId);
	
	/**
	 * 获取菜单树
	 * @param roleId
	 * @return
	 */
	List<Map<String, Object>> getMenuTree(Integer roleId);
	
	/**
	 * 获取所有菜单树
	 * @return
	 */
	List<Map<String, Object>> getMenuTreeAll();
	
	/**
	 * 根据ID删除菜单
	 * @param id
	 * @return
	 */
	ResultMessage<?> delete(Integer id);
	
	/**
	 * 根据ID更新菜单基本信息
	 * @param menu
	 * @return
	 */
	ResultMessage<?> update(Menu menu);
	
	/**
	 * 新增菜单
	 * @param menu
	 * @return
	 */
	ResultMessage<?> addMenu(Menu menu);
}
