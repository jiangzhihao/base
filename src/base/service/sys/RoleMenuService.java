package base.service.sys;

import base.util.ResultMessage;

/**
 * 
 * RoleMenuService
 *
 * @author 姜治昊
 * @time 2018年1月4日 下午10:22:20
 */
public interface RoleMenuService {

	/**
	 * 更新角色菜单关系
	 * @param ids 菜单ID
	 * @param roleId 角色ID
	 * @return
	 */
	ResultMessage<?> updateRoleMenu(String ids, Integer roleId);
	
	/**
	 * 删除角色菜单关联关系
	 * @param num
	 * @param menuId
	 * @param ids
	 * @return
	 */
	ResultMessage<?> deleteMenuRoles(int num, Integer menuId, String ids); 
	
	/**
	 * 为菜单挂接多个角色
	 * @param ids
	 * @param menuId
	 * @param num
	 * @return
	 */
	ResultMessage<?> addRoles(String ids, Integer menuId, int num);
	
}
