package base.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import base.entity.Role;

/**
 * 
 * RoleDao
 *
 * @author 姜治昊
 * @time 2018年1月3日 下午5:23:58
 */
public interface RoleDao {

	/**
	 * 获取角色列表
	 * @param name
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<Role> getRoleList(@Param("name") String name, @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	/**
	 * 数据数量
	 * @param name
	 * @return
	 */
	int getRoleListCount(@Param("name") String name); 
	
	/**
	 * 添加角色
	 * @param role
	 * @return
	 */
	int addRole(Role role);
	
	/**
	 * 更新角色
	 * @param role
	 * @return
	 */
	int updateRole(Role role);
	
	/**
	 * 批量删除
	 * @param ids
	 * @return
	 */
	int deleteRoles(@Param("ids") String ids);
	
	/**
	 * 根据ID获取Role
	 * @param id
	 * @return
	 */
	Role getRoleById(@Param("id") Integer id);
	
	/**
	 * 获取不属于指定用户的角色
	 * @param userId
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<Role> getRolesNotinUser(@Param("userId") Integer userId,  @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	/**
	 * 数量
	 * @param userId
	 * @return
	 */
	int getRolesNotinUserCount(@Param("userId") Integer userId);
	
	/**
	 * 根据菜单获取角色
	 * @param isLeaf
	 * @param menuId
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<Role> getRolesByMenu(@Param("isLeaf") int isLeaf, @Param("menuId") Integer menuId,  @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	/**
	 * 数量
	 * @param isLeaf
	 * @param menuId
	 * @return
	 */
	int getRolesByMenuCount(@Param("isLeaf") int isLeaf, @Param("menuId") Integer menuId);
	
	/**
	 * 获取没有与指定菜单关联的所有角色
	 * @param menuId
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<Role> getRolesNotinMenu(@Param("menuId") Integer menuId,  @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	/**
	 * 数量
	 * @param menuId
	 * @return
	 */
	int getRolesNotinMenuCount(@Param("menuId") Integer menuId);
}
