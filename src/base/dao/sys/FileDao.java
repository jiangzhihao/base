package base.dao.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 文件
 * @author JZH
 *
 */
public interface FileDao {

	int saveFile(@Param("fileName") String name, @Param("filePath") String path, @Param("fileInfo") String info, @Param("userId") Integer userId);
	
	List<Map<String, Object>> allFiles();
	
	Map<String, Object> getPathAndNameById(@Param("fileId") Integer id);
}
