package base.dao.sys;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import base.entity.Log;

/**
 * 
 * LogDao
 *
 * @author 姜治昊
 * @time 2018年2月1日 下午10:38:21
 */
public interface LogDao {

	/**
	 * 保存日志记录
	 * @param log
	 * @return
	 */
	int saveLog(Log log);
	
	/**
	 * 查询日志记录
	 * @param userName
	 * @param type
	 * @param startTime
	 * @param endTime
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<Log> getLogs(@Param("userName") String userName, @Param("type") String type, @Param("startTime") Date startTime, @Param("endTime") Date endTime, @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	
	/**
	 * 数量
	 * @param userName
	 * @param type
	 * @param startTime
	 * @param endTime
	 * @return
	 */
	int getLogsCount(@Param("userName") String userName, @Param("type") String type, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
}
