package base.dao.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import base.entity.Menu;

/**
 * 
 * MenuDao
 *
 * @author 姜治昊
 * @time 2017年12月16日 下午11:41:05
 */
public interface MenuDao {

	/**
	 * 根据ID获取菜单详细信息
	 * @param menuId
	 * @return
	 */
	Menu getMenuById(@Param("menuId") Integer menuId);
	
	/**
	 * 根据用户获取对应的菜单
	 * @param id
	 * @return
	 */
	List<Menu> getMenuByUser(@Param("userId") Integer id);
	
	/**
	 * 根据PID和用户权限获取菜单
	 * @param id
	 * @param userId
	 * @return
	 */
	List<Menu> getMenuByUserAndPid(@Param("pid") Integer id, @Param("userId") Integer userId);
	
	/**
	 * 根据PID获取菜单
	 * @param id
	 * @return
	 */
	List<Menu> getMenuByPid(@Param("pid") Integer id);
	
	/**
	 * 获取子节点（封装成Tree）
	 * @param pid
	 * @param roleId
	 * @return
	 */
	List<Map<String, Object>> getMenuByPidInTree(@Param("pid") Integer pid, @Param("roleId") Integer roleId);
	
	/**
	 * 获取子节点（封装成Tree）
	 * @param pid
	 * @return
	 */
	List<Map<String, Object>> getMenuByPidInTreeAll(@Param("pid") Integer pid);
	
	/**
	 * 获取菜单树
	 * @param roleId
	 * @return
	 */
	List<Map<String, Object>> getMenuTree(@Param("roleId") Integer roleId);
	
	/**
	 * 获取所有菜单树
	 * @return
	 */
	List<Map<String, Object>> getMenuTreeAll();
	
	/**
	 * 根据ID删除菜单
	 * @param id
	 * @return
	 */
	int delete(@Param("menuId") Integer id);
	
	/**
	 * 根据ID更新菜单基本信息
	 * @param menu
	 * @return
	 */
	int update(Menu menu);
	
	/**
	 * 新增菜单
	 * @param menu
	 * @return
	 */
	int addMenu(Menu menu);
	
}
