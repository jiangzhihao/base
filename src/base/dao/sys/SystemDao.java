package base.dao.sys;

import base.entity.SystemProp;

/**
 * 
 * SystemDao
 *
 * @author 姜治昊
 * @time 2018年2月3日 下午3:13:16
 */
public interface SystemDao {

	/**
	 * 获取系统初始化参数
	 * @return
	 */
	SystemProp initSystem();
	
}
