package base.dao.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

/**
 * 
 * RoleMenuDao
 *
 * @author 姜治昊
 * @time 2018年1月4日 下午10:26:32
 */
public interface RoleMenuDao {

	/**
	 * 添加角色菜单关系
	 * @param params :
	 *  key            value
	 *  roleId		   角色ID
	 *  menuId        菜单ID
	 * @return
	 */
	int addRoleMenu(List<Map<String, Object>> params);
	
	/**
	 * 为菜单挂接多个角色
	 * @param ids
	 * @param menuId
	 * @return
	 */
	int addRoles(@Param("ids") List<Integer> ids, @Param("menuId") Integer menuId);
	
	/**
	 * 删除角色菜单关系
	 * @param roleId 角色ID
	 * @param ids 菜单ID（用“,”分割）
	 * @return
	 */
    int deleteRoleMenu(@Param("roleId") Integer roleId, @Param("ids") String ids);
    
    /**
     * 根据角色获取对应的菜单ID
     * @param roleId 角色ID
     * @return
     */
    List<Integer> getMenuIdsByRole(@Param("roleId") Integer roleId);
    
    /**
     * 删除角色下的所有 角色菜单 关联关系
     * @param ids
     * @return
     */
    int deleteRoleMenuByRole(@Param("ids") String ids);
    
    /**
     * 删除关联关系
     * @param menuId 菜单ID
     * @param ids 角色ID（用“,”分割）
     * @return
     */
    int delete(@Param("menuId") Integer menuId, @Param("ids") String ids);
	
}
