package base.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import base.entity.Department;

/**
 * 
 * DepartmentDao
 *
 * @author 姜治昊
 * @time 2017年12月13日 下午9:51:51
 */
public interface DepartmentDao {

	/**
	 * 根据ID获取部门
	 * @param id
	 * @return
	 */
	Department getDeptById(@Param("deptId") Integer id);
	
	/**
	 * 获取部门列表
	 * @return
	 */
	List<Department> getDeptList();
	
}
