package base.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import base.entity.User;

/**
 * 
 * UserDao
 *
 * @author 姜治昊
 * @time 2017年12月13日 下午9:35:05
 */
public interface UserDao {

	/**
	 * 登录认证
	 * @param loginname
	 * @param password
	 * @return
	 */
	User authentication(@Param("loginname") String loginname, @Param("password") String password);
	
	/**
	 * 获取用户列表（无条件、基本信息）
	 * @param userName
	 * @param loginName
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<User> getUserList(@Param("userName") String userName, @Param("loginName") String loginName, @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	/**
	 * 获取不属于某角色的下用户列表
	 * @param roleId
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<User> getUserNotInRoleById(@Param("roleId") Integer roleId, @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	/**
	 * 数量
	 * @param roleId
	 * @return
	 */
	int getUserNotInRoleByIdCount(@Param("roleId") Integer roleId);
	
	/**
	 * 数量
	 * @param userName
	 * @param loginName
	 * @return
	 */
	int getUserListCount(@Param("userName") String userName, @Param("loginName") String loginName);
	
	/**
	 * 批量删除用户
	 * @param ids
	 * @return
	 */
	int deleteUsers(@Param("ids") String ids);
	
	/**
	 * 根据ID获取用户
	 * @param id
	 * @return
	 */
	User getUserById(@Param("id") Integer id);
	
	/**
	 * 修改用户基本信息
	 * @param user
	 * @return
	 */
	int updateUser(User user);
	
	/**
	 * 添加用户
	 * @param user
	 * @return
	 */
	int addUser(User user);
}
