package base.dao.sys;

import java.util.List;

import org.apache.ibatis.annotations.Param;

/**
 * 
 * UserDeptDao
 *
 * @author 姜治昊
 * @time 2018年1月18日 下午6:05:29
 */
public interface UserDeptDao {

	/**
	 * 添加用户-部门 关联关系
	 * @param depts
	 * @param userId
	 * @return
	 */
	int addUserDepts(@Param("deptIds") List<Integer> depts, @Param("userId") Integer userId);
	
}
