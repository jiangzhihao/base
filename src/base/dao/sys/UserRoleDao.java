package base.dao.sys;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import base.entity.Role;
import base.entity.User;

/**
 * 
 * UserDao
 *
 * @author 姜治昊
 * @time 2017年12月13日 下午9:35:05
 */
public interface UserRoleDao {

	/**
	 * 根据角色获取对应的用户
	 * @param id
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<User> getUserByRoleId(@Param("id") Integer id, @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	/**
	 * 数量
	 * @param id
	 * @return
	 */
	int getUserByRoleIdCount(@Param("id") Integer id);
	
	/**
	 * 批量移除用户角色关系
	 * @param ids
	 * @param roleId
	 * @return
	 */
	int deleteUserRoleById(@Param("ids") String ids, @Param("roleId") Integer roleId);
	
	/**
	 * 删除指定角色下的所有 用户-角色 关联关系
	 * @param ids
	 * @return
	 */
	int deleteUserRoleByRole(@Param("ids") String ids);
	
	/**
	 * 为角色批量添加用户
	 * @param params
	 * @return
	 */
	int addRoleUsers(List<Map<String, Object>> params);
	
	/**
	 * 为用户批量添加角色
	 * @param params
	 * @return
	 */
	int addUserRoles(List<Map<String, Object>> params);
	
	/**
	 * 根据用户获取对应的角色
	 * @param userId
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	List<Role> getRolesByUser(@Param("userId") Integer userId, @Param("startRow") int startRow, @Param("endRow") int endRow);
	
	/**
	 * 数量
	 * @param userId
	 * @return
	 */
	int getRolesByUserCount(@Param("userId") Integer userId);
	
	/**
	 * 删除用户下的角色
	 * @param userId
	 * @param ids
	 * @return
	 */
	int deleteRolesByUser(@Param("userId") Integer userId, @Param("ids") String ids);
}
