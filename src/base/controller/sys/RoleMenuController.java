package base.controller.sys;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import base.anno.Logger;
import base.service.sys.RoleMenuService;
import base.util.ResponseUtil;

/**
 * 
 * RoleMenuController
 *
 * @author 姜治昊
 * @time 2018年1月7日 下午5:59:05
 */
@Controller
@RequestMapping("sys/rolemenu")
public class RoleMenuController {

	@Autowired
	private RoleMenuService roleMenuService;
	
	/**
	 * 更新 角色-菜单关系
	 * @param response
	 * @param ids 菜单ID
	 * @param roleId 修改的角色ID
	 */
	@Logger(description = "为角色更新多条菜单关联关系")
	@RequestMapping(value="update", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void updateRoleMenu(HttpServletResponse response, String ids, Integer roleId) {
		ResponseUtil.write(response, JSONObject.fromObject(roleMenuService.updateRoleMenu(ids, roleId)));
	}
	
	/**
	 * 移除记录
	 * @param response
	 * @param ids
	 * @param menuId
	 * @param num
	 */
	@Logger(description = "删除指定菜单的所条角色")
	@RequestMapping(value="delete", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void deleteByMenuId(HttpServletResponse response, String ids, Integer menuId, int num) {
		ResponseUtil.write(response, JSONObject.fromObject(roleMenuService.deleteMenuRoles(num, menuId, ids)));
	}
	
	/**
	 * 为菜单挂接多个角色记录
	 * @param response
	 * @param ids
	 * @param menuId
	 * @param num
	 */
	@Logger(description = "为指定菜单添加所条角色")
	@RequestMapping(value="addRoles", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void addRoles(HttpServletResponse response, String ids, Integer menuId, int num) {
		ResponseUtil.write(response, JSONObject.fromObject(roleMenuService.addRoles(ids, menuId, num)));
	}
}
