package base.controller.sys;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import base.anno.Logger;
import base.anno.LoginLogger;
import base.entity.User;
import base.service.sys.SystemService;
import base.service.sys.UserService;
import base.util.Md5Util;
import base.util.ResponseUtil;
import base.util.ResultMessage;
import base.util.StaticConstant;

/**
 * 
 * UserController
 *
 * @author 姜治昊
 * @time 2017年12月13日 下午9:26:54
 */
@Controller
@RequestMapping("sys/user")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private SystemService systemService;
	
	/**
	 * 去用户管理首页
	 */
	@RequestMapping(value="manager", method=RequestMethod.GET)
	public String gotoManagerPage() {
		return "sys/manager/user";
	}
	
	/**
	 * 无条件查询用户列表
	 * @param response
	 */
	@RequestMapping(value="list", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getUserList(HttpServletResponse response, String userName, String loginName, int page, int rows) {
		ResponseUtil.write(response, userService.getUserList(userName, loginName, page, rows));
	}
	
	/**
	 * 登录认证
	 * @param response
	 * @param loginname
	 * @param password
	 */
	@LoginLogger
	@RequestMapping(value="authe", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void authentication(HttpServletResponse response, HttpSession session, 
			@RequestParam("loginname") String loginname, @RequestParam("password") String password) {
		User user = userService.authentication(loginname, Md5Util.toMD5(password));
		if(null != user && null != user.getDept()) {
			session.setAttribute("LOGIN_USER", user);
			session.setAttribute("SYSTEM_PROP", systemService.getSystemProps(StaticConstant.CACHE_SYSTEM_PROP_KEY));
			ResponseUtil.write(response, JSONObject.fromObject(ResultMessage.createSuccessResultMessage()));
		} else {
			ResponseUtil.write(response, JSONObject.fromObject(ResultMessage.createErrorResultMessage("账号或密码错误")));
		}
	}
	
	/**
	 * 获取不属于某个角色下的用户
	 * @param response
	 */
	@RequestMapping(value="notinrole/{roleId}", method=RequestMethod.POST, produces="text/html;charset=UTF-8")
	public void getUserNotInRoleById(HttpServletResponse response, @PathVariable("roleId") Integer roleId, int page, int rows) {
		ResponseUtil.write(response, userService.getUserNotInRoleById(roleId, page, rows));
	}
	
	/**
	 * 批量删除用户
	 * @param response
	 * @param ids
	 * @param num
	 */
	@Logger(description = "批量删除用户")
	@RequestMapping(value="del", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void deleteUsers(HttpServletResponse response, String ids, int num) {
		ResponseUtil.write(response, JSONObject.fromObject(userService.deleteUsers(ids, num)));
	}
	
	/**
	 * 获取用户基本信息（编辑）
	 * @param response
	 * @param id
	 */
	@RequestMapping(value="getUserById", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getUserById(HttpServletResponse response, Integer id) {
		ResponseUtil.write(response, JSONObject.fromObject(userService.getUserById(id)));
	}
	
	/**
	 * 更新用户基本信息
	 * @param response
	 * @param user
	 */
	@Logger(description = "更新用户基本信息")
	@RequestMapping(value="updateUser", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void updateUser(HttpServletResponse response, User user) {
		ResponseUtil.write(response, JSONObject.fromObject(userService.updateUser(user)));
	}
	
	/**
	 * 添加用户
	 * @param response
	 * @param user
	 */
	@Logger(description = "添加用户")
	@RequestMapping(value="addUser", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void addUser(HttpServletResponse response, User user) {
		ResponseUtil.write(response, JSONObject.fromObject(userService.addUser(user)));
	}
	
	/**
	 * 登出
	 * @param request
	 */
	@RequestMapping(value="logout", method=RequestMethod.GET, produces="text/html;charset=UTF-8")
	public String logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return "sys/login";
	}
	
}
