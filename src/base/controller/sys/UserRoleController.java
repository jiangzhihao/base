package base.controller.sys;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import base.anno.Logger;
import base.service.sys.UserRoleService;
import base.util.ResponseUtil;

/**  
 * 
 * UserRoleController
 *
 * @author 姜治昊
 * @time 2018年1月4日 下午2:07:06
 */
@Controller
@RequestMapping("sys/userrole")
public class UserRoleController {
	
	@Autowired
	private UserRoleService userRoleService;

	/**
	 * 根据角色获取用户列表
	 * @param response
	 * @param roleId
	 * @param page
	 * @param rows
	 */
	@RequestMapping(value="list", method=RequestMethod.POST, produces="text/html;charset=UTF-8")
	public void getUserByRoleId(HttpServletResponse response, Integer roleId, int page, int rows) {
		ResponseUtil.write(response, userRoleService.getUserByRoleId(roleId, page, rows));
	}
	
	/**
	 * 批量移除用户角色关联信息
	 * @param response
	 * @param ids
	 */
	@Logger(description = "删除指定角色的多条用户")
	@RequestMapping(value="delete", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void deleteUserRoleById(HttpServletResponse response, int num, String ids, Integer roleId) {
		ResponseUtil.write(response, JSONObject.fromObject(userRoleService.deleteUserRoleById(num, ids, roleId)));
	}
	
	/**
	 * 为角色批量添加用户
	 * @param roleId
	 * @param num
	 * @param ids
	 */
	@Logger(description = "为指定角色添加多条用户")
	@RequestMapping(value="add", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void addRoleUsers(HttpServletResponse response, Integer roleId, int num, String ids) {
		ResponseUtil.write(response, JSONObject.fromObject(userRoleService.addRoleUsers(roleId, num, ids)));
	}
	
	/**
	 * 为用户批量添加角色
	 * @param response
	 * @param userId
	 * @param num
	 * @param ids
	 */
	@Logger(description = "为指定用户添加多条角色")
	@RequestMapping(value="addByUser", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void addRoleUsersByUser(HttpServletResponse response, Integer userId, int num, String ids) {
		ResponseUtil.write(response, JSONObject.fromObject(userRoleService.addRoleUsersByUser(userId, num, ids)));
	}
	
	/**
	 * 根据用户获取对应的角色
	 * @param response
	 * @param userId
	 */
	@RequestMapping(value="getRolesByUser/{userId}", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getRolesByUser(HttpServletResponse response, @PathVariable("userId") Integer userId, int page, int rows) {
		ResponseUtil.write(response, userRoleService.getRolesByUser(userId, page, rows));
	}
	
	/**
	 * 删除用户下的角色
	 * @param response
	 * @param userId
	 * @param roleId
	 */
	@Logger(description = "删除指定用户的多条角色")
	@RequestMapping(value="deleteRolesByUser", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void deleteRolesByUser(HttpServletResponse response, Integer userId, String ids, int num) {
		ResponseUtil.write(response, JSONObject.fromObject(userRoleService.deleteRolesByUser(userId, ids, num)));
	}
	
}
