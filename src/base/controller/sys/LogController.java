package base.controller.sys;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import base.service.sys.LogService;
import base.util.DateUtil;
import base.util.ResponseUtil;

/**
 * 
 * LogController
 *
 * @author 姜治昊
 * @time 2018年2月1日 下午11:14:49
 */
@Controller
@RequestMapping("sys/log")
public class LogController {

	@Autowired
	private LogService logService;
	
	/**
	 * 去用户管理首页
	 */
	@RequestMapping(value="manager", method=RequestMethod.GET)
	public String gotoManagerPage() {
		return "sys/manager/log";
	}
	
	/**
	 * 查询日志
	 * @param response
	 * @param user
	 * @param startTime
	 * @param endTime
	 * @param page
	 * @param rows
	 */
	@RequestMapping(value="getLogs", method=RequestMethod.POST, produces="text/html;charset=UTF-8")
	public void getLogList(HttpServletResponse response, String userName, String type, String startTime, String endTime, int page, int rows) {
		ResponseUtil.write(response, logService.getLogs(
				  userName
			    , type
				, (startTime != null && startTime.length() > 0)? DateUtil.formatStringToDateDate(startTime):null
				, (endTime != null && endTime.length() > 0)? DateUtil.formatStringToDateDate(endTime):null
				, page, rows));
	}
	
}
