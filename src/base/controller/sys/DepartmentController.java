package base.controller.sys;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONArray;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import base.service.sys.DeptService;
import base.util.ResponseUtil;

/**
 * 
 * DepartmentController
 *
 * @author 姜治昊
 * @time 2018年1月17日 下午3:43:09
 */
@Controller
@RequestMapping("sys/dept")
public class DepartmentController {
	
	@Autowired
	private DeptService deptService;
	
	/**
	 * 获取部门列表
	 * @param response
	 */
	@RequestMapping(value="list", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getDeptList(HttpServletResponse response) {
		ResponseUtil.write(response, JSONArray.fromObject(deptService.getDeptList()));
	}

}
