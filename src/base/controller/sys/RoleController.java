package base.controller.sys;

import javax.servlet.http.HttpServletResponse;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import base.anno.Logger;
import base.entity.Role;
import base.service.sys.RoleService;
import base.util.ResponseUtil;

/**
 * 角色
 * RoleController
 *
 * @author 姜治昊
 * @time 2018年1月3日 下午5:10:53
 */
@Controller
@RequestMapping("sys/role")
public class RoleController {
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value="manager", method=RequestMethod.GET)
	public String gotoManagerPage() {
		return "sys/manager/role";
	}
	
	/**
	 * 获取角色列表
	 * @param response
	 * @param name
	 * @param page 初始 1
	 * @param rows 初始 15
	 */
	@RequestMapping(value="list", method=RequestMethod.POST, produces="text/html;charset=UTF-8")
	public void getRoleList(HttpServletResponse response, String name, int page, int rows) {
//		ResponseUtil.write(response, JSONArray.fromObject(roleService.getRoleList(name, page, rows)));
		ResponseUtil.write(response, roleService.getRoleList(name, page, rows));
	}
	
	/**
	 * 添加
	 * @param role
	 */
	@Logger(description = "单条添加角色")
	@RequestMapping(value="add", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void addRole(HttpServletResponse response, Role role) {
		ResponseUtil.write(response, JSONObject.fromObject(roleService.addRole(role)));
	}
	
	/**
	 * 修改
	 * @param response
	 * @param role
	 */
	@Logger(description = "修改角色基本信息")
	@RequestMapping(value="update", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void updateRole(HttpServletResponse response, Role role) {
		ResponseUtil.write(response, JSONObject.fromObject(roleService.updateRole(role)));
	}
	
	/**
	 * 删除
	 * @param response
	 * @param num
	 * @param ids
	 */
	@Logger(description = "批量删除角色")
	@RequestMapping(value="delete", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void deleteRole(HttpServletResponse response, int num, String ids) {
		ResponseUtil.write(response, JSONObject.fromObject(roleService.deleteRoles(num, ids)));
	}
	
	/**
	 * 根据ID获取
	 * @param response
	 * @param id
	 */
	@RequestMapping(value="getRoleById", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getRoleById(HttpServletResponse response, Integer id) {
		ResponseUtil.write(response, JSONObject.fromObject(roleService.getRoleById(id)));
	}
	
	/**
	 * 获取用户没有的角色
	 * @param response
	 * @param userId
	 * @param page
	 * @param rows
	 */
	@RequestMapping(value="getRolesNotinUser", method=RequestMethod.POST, produces="text/html;charset=UTF-8")
	public void getRolesNotinUser(HttpServletResponse response, Integer userId, int page, int rows) {
		ResponseUtil.write(response, roleService.getRolesNotinUser(userId, page, rows));
	}
	
	/**
	 * 获取没有与指定菜单关联的角色
	 * @param response
	 * @param menuId
	 * @param page
	 * @param rows
	 */
	@RequestMapping(value="getRolesNotinMenu", method=RequestMethod.POST, produces="text/html;charset=UTF-8")
	public void getRolesNotinMenu(HttpServletResponse response, Integer menuId, int page, int rows) {
		ResponseUtil.write(response, roleService.getRolesNotinMenu(menuId, page, rows));
	}
	
	/**
	 * 通过菜单获取角色
	 * @param response
	 * @param menuId 有可能是父节点，也有可能是叶子节点；两者查询方式不同（因为父节点不会与角色进行关联）
	 * @param page
	 * @param rows
	 */
	@RequestMapping(value="getRolesByMenu", method=RequestMethod.POST, produces="text/html;charset=UTF-8")
	public void getRolesByMenu(
			HttpServletResponse response, @RequestParam(value="isLeaf", required=false,defaultValue= "0") Integer isLeaf
			, @RequestParam(value="menuId", required=false, defaultValue="0") Integer menuId, int page, int rows
		) {
		ResponseUtil.write(response, roleService.getRolesByMenu(isLeaf, menuId, page, rows));
	}
	

}
