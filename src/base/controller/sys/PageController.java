package base.controller.sys;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import base.service.sys.FileService;
import base.service.sys.UserService;

/**
 * 
 * PageController
 *
 * @author 姜治昊
 * @time 2017年12月13日 下午10:59:07
 */
@Controller
@RequestMapping("sys/page")
public class PageController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private FileService fileService;
	
	/**
	 * 进入登录界面
	 * @return
	 */
	@RequestMapping(value="login", method=RequestMethod.GET)
	public String gotoLogin() {
		return "sys/login";
	}
	
	/**
	 * 进入主界面
	 * @return
	 */
	@RequestMapping(value="main", method=RequestMethod.GET)
	public String gotoMain() {
		return "sys/main";
	}
	
	/**
	 * 测试，文件上传下载界面
	 * @return
	 */
	@RequestMapping(value="file", method=RequestMethod.GET)
	public String file(HttpServletRequest request) {
		List<Map<String, Object>> maps = fileService.allFiles();
		request.setAttribute("files", maps);
		return "sys/file";
	}
	
}
