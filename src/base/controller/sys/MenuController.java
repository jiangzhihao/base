package base.controller.sys;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import base.anno.Logger;
import base.entity.Menu;
import base.entity.User;
import base.service.sys.MenuService;
import base.util.ResponseUtil;
import base.util.ResultMessage;

/**
 * 
 * MenuController
 *
 * @author 姜治昊
 * @time 2017年12月17日 下午2:09:20
 */
@Controller
@RequestMapping("sys/menu")
public class MenuController {

	@Autowired
	private MenuService menuService;
	
	/**
	 * 跳转页面
	 * @return
	 */
	@RequestMapping(value="manager", method=RequestMethod.GET)
	public String gotoManagerPage() {
		return "sys/manager/menu";
	}
	
	/**
	 * 删除菜单
	 * @param response
	 * @param menuId
	 */
	@Logger(description = "单条删除菜单")
	@RequestMapping(value="delete", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void deleteMenuById(HttpServletResponse response, Integer menuId) {
		ResponseUtil.write(response, JSONObject.fromObject(menuService.delete(menuId)));
	}
	
	/**
	 * 更新菜单
	 * @param response
	 * @param menu
	 */
	@Logger(description = "单条更新菜单基本信息")
	@RequestMapping(value="update", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void updateMenuById(HttpServletResponse response, Menu menu) {
		ResponseUtil.write(response, JSONObject.fromObject(menuService.update(menu)));
	}
	
	/**
	 * 新增菜单
	 * @param response
	 * @param menu
	 */
	@Logger(description = "新增菜单")
	@RequestMapping(value="add", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void addMenu(HttpServletResponse response, Menu menu) {
		ResponseUtil.write(response, JSONObject.fromObject(menuService.addMenu(menu)));
	}
	
	/**
	 * 根据ID获取菜单详细信息
	 * @param response
	 * @param id
	 */
	@RequestMapping(value="getMenuById/{id}", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getMenuById(HttpServletResponse response, @PathVariable("id") Integer id){
		ResponseUtil.write(response, JSONObject.fromObject(menuService.getMenuById(id)));
	}
	
	/**
	 * 根据登录用户获取对应的菜单
	 */
	@RequestMapping(value="getMenuByUsers", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getMenuByUser(HttpServletResponse response, HttpSession session) {
		User user = (User) session.getAttribute("LOGIN_USER");
		List<Menu> menus = menuService.getUserMenus(user.getId());
		ResponseUtil.write(response, JSONObject.fromObject(ResultMessage.createSuccessResultMessage(menus)));
	}
	
	/**
	 * 根据角色获取菜单树
	 * @param response
	 */
	@RequestMapping(value="tree/{roleId}", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getMenuTree(HttpServletResponse response, @PathVariable("roleId") Integer roleId) {
		ResponseUtil.write(response, JSONArray.fromObject(menuService.getMenuTree(roleId)));
	}
	
	/**
	 * 获取所有菜单树
	 * @param response
	 */
	@RequestMapping(value="treeAll", method=RequestMethod.POST, produces="application/json;charset=UTF-8")
	public void getMenuTreeAll(HttpServletResponse response) {
		ResponseUtil.write(response, JSONArray.fromObject(menuService.getMenuTreeAll()));
	}
	
}
