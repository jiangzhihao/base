package base.datasource;

/**
 * 
 * DataSourceContextHolder:获得和设置上下文环境 主要负责改变上下文数据源的名称 
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午11:32:40
 */
public class DataSourceContextHolder {
	
	/**
	 * 线程本地环境
	 */
	private static final ThreadLocal<Object> CONTEXT_HOLDER = new ThreadLocal<Object>();   
	  
    /**
     *  设置数据源类型  
     * @param dataSourceType
     */
    public static void setDataSourceType(String dataSourceType) {  
    	CONTEXT_HOLDER.set(dataSourceType);  
    }  
  
    /**
     *  获取数据源类型  
     * @return
     */
    public static String getDataSourceType() {  
        return (String) CONTEXT_HOLDER.get();  
    }  
  
    /**
     *  清除数据源类型  
     */
    public static void clearDataSourceType() {  
    	CONTEXT_HOLDER.remove();  
    }  
}
