package base.datasource;

/**
 * 
 * DataSourceConst:
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午11:31:54
 */
public class DataSourceConst {
	
	public static final String DEFAULT_DATASOURCE="defaultDataSource";  
    public static final String DM_DATASOURCE="DMDataSource";  
    public static final String ORACLE_DATASOURCE="OracleDataSource";
    public static final String MYSQL_DATASOURCE="MySQLDataSource";
    public static final String SQLSERVER_DATASOURCE="SQLServerDataSource";
    
    
}
