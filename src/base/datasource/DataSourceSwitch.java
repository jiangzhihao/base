package base.datasource;

import org.springframework.util.Assert;

/**
 * 
 * DataSourceSwitch: 数据源切换
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午11:33:20
 */
public class DataSourceSwitch {

	private static final ThreadLocal<DataSourceType> CONTEXT_HOLDER = new ThreadLocal<DataSourceType>();

	public static void setDataSourceType(DataSourceType dataSourceType) {
		Assert.notNull(dataSourceType, "数据源类型不能为null！");
		CONTEXT_HOLDER.set(dataSourceType);
	}

	public static DataSourceType getDataSourceType() {
		return CONTEXT_HOLDER.get() == null ? DataSourceType.DEFAULT
				: (DataSourceType) CONTEXT_HOLDER.get();
	}

	public static void clearDataSourceType() {
		CONTEXT_HOLDER.remove();
	}

}
