package base.datasource;

/**
 * 
 * DataSourceSwitchHelper: 数据源切换帮助类 ,用于aop切换数据源
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午11:33:36
 */
public class DataSourceSwitchHelper {

	public void switchDefault() {
		DataSourceSwitch.setDataSourceType(DataSourceType.DEFAULT);
	}

}
