package base.datasource;

/**
 * 
 * DataSourceType: 数据源类型
 * 				      格式：数据库名（数据库类型）
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午11:34:07
 */
public enum DataSourceType {
	
	/**
	 * 数据库名
	 */
	DEFAULT("mysql");

	private String dialect;

	private DataSourceType(String dialect) {
		this.dialect = dialect;
	}

	public String getDialect() {
		return dialect;
	}

	public static void main(String[] args) {
		System.out.println(DataSourceType.DEFAULT.getDialect());

	}

}
