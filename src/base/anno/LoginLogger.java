package base.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 
 * LoginLogger
 * 登陆日志
 *
 * @author 姜治昊
 * @time 2018年2月1日 下午9:54:02
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface LoginLogger {

	
	
}
