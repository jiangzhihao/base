package base.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * DateUtil:
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午10:39:34
 */
public class DateUtil {

	
	/**
	 * 日期类型转换成字符串（yyyy-MM-dd）
	 * @param date
	 * @return
	 */
	public static String formatDateToStringDate(Date date) {
		String result = "";
		if(null != date) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			result = sdf.format(date);
		}
		return result;
	}
	
	/**
	 * 日期类型转换成字符串(yyyy-MM-dd HH:mm:ss)
	 * @param date
	 * @return
	 */
	public static String formatDateToStringTimestamp(Date date) {
		String result = "";
		if(null != date) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			result = sdf.format(date);
		}
		return result;
	}
	
	/**
	 * ����ת�����ַ��� ����ʽ�Զ��壩
	 * @param date
	 * @param str �ַ�����ʽ
	 * @return
	 */
	public static String formatDateToStringByString(Date date,String str) {
		String result = "";
		if(null != date) {
			SimpleDateFormat sdf = new SimpleDateFormat(str);
			result = sdf.format(date);
		}
		return result;
	}
	
	/**
	 * 字符串转换成日期类型(yyyy-MM-dd)
	 * @param str
	 * @return
	 */
	public static Date formatStringToDateDate(String str) {
		Date date = null;
		if(str == null || str == "" || str.length() <= 0){
			return null;
		}else{
			try {
				date = new SimpleDateFormat("yyyy-MM-dd").parse(str);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return date;
		}
	}
	
	/**
	 * 字符串转换成日期类型(yyyy-MM-dd HH-mm-ss)
	 * @param str
	 * @return
	 */
	public static Date formatStringToDateTimestamp(String str) {
		Date date = null;
		if(str == null || str == "" || str.length() <= 0){
			return null;
		}else{
			try{
				date = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").parse(str);
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
			return date;
		}	
	}
	public static Date formatTimestampStringToDate(String str) {
		Date date = null;
		if(str == null || str == "" || str.length() <= 0){
			return null;
		}else{
			try{
				date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(str);
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
			return date;
		}	
	}
	
	/**
	 * �ַ���ת�������ڣ���ʽ�Զ��壩
	 * @param str1
	 * @param str2 ���ڸ�ʽ
	 * @return
	 */
	public static Date formatStringToDateByString(String str1,String str2) {
		Date date = null;
		if(str1 == null || str1 == "" || str1.length() <= 0){
			return null;
		}else{
			try{
				date = new SimpleDateFormat(str2).parse(str1);
			} catch(Exception e) {
				e.printStackTrace();
				return null;
			}
			return date;
		}	
	}
	
	/**
	 * ��ʽת��
	 * @param date
	 * @return
	 */
	public static String replaceDate(String date) {
		String s1 = date.substring(0, 4);
		String s2 = date.substring(5,7);
		String s3 = date.substring(8);
		return s2+"/"+s3+"/"+s1;
    }
	
	/**
	 * ���ؼ���
	 */
	public static int getMonth(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH)+1;
	}
	
	/**
	 * ���ؼ���
	 */
	public static int getDay(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DATE);
	}
	
	public static int getYear(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}
	
	public static int getHour(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.HOUR_OF_DAY);
	}
	
	public static int getMinute(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MINUTE);
	}
	
	/**
	 * �������ڼ�
	 * @param date
	 *  @return 1:����һ
	 *          6:������
	 *          7:������
	 */
	public static int getWeek(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return (calendar.get(Calendar.DAY_OF_WEEK)-1);
	}
	
	/**
	 * ����������ָ��������
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date addDate(Date date,int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, day);
		return calendar.getTime();
	}
	
	public static Date addHour(Date date,int hour) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, hour);
		return calendar.getTime();
	}
	
	/**
	 * �����ڼ�ȥָ��������
	 * @param date
	 * @param day
	 * @return
	 */
	public static Date delDate(Date date,int day) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, -(day));
		return calendar.getTime();
	}
	
	public static Date delHour(Date date,int hour) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.HOUR_OF_DAY, -(hour));
		return calendar.getTime();
	}
	
	/**
	 * �Ƚ�Date���ʹ�С
	 * @param jobDate
	 * @param now
	 * @return
	 */
	public static boolean preparedDate(Date jobDate,Date now) {
		long jobm = jobDate.getTime();
		long nowm = now.getTime();
		if(jobm >= nowm) {
			return false;
		} else {
			return true;
		}
	}
}
