package base.util;

/**
 * 定义静态常量
 * StaticConstant
 *
 * @author 姜治昊
 * @time 2018年2月3日 下午8:37:18
 */
public class StaticConstant {

	/**
	 * 系统基本信息在缓存中的key
	 */
	public static final String CACHE_SYSTEM_PROP_KEY = "SYSTEM_PROPS";
	
}
