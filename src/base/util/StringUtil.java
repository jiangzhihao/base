package base.util;

/**
 * 自定义字符串工具类
 * StringUtil
 *
 * @author 姜治昊
 * @time 2017年12月4日 上午10:49:42
 */
public class StringUtil {

	/**
	 * 根据 - 分割
	 * @param str
	 * @return
	 */
	public static String[] strToArr(String str) {
		if(null != str && str.length() > 0) {
			String[] arr = str.split("-");
			return arr;
		} else {
			throw new NullPointerException("无效的待分割字符串");
		}
	}
	
	/**
	 * 根据 - 分割
	 * @param str
	 * @return
	 */
	public static Integer[] strToIntArr(String str) {
		if(null != str && str.length() > 0) {
			String[] arr = str.split("-");
			Integer[] result = new Integer[arr.length];
			for(int i=0;i<arr.length;i++) {
				result[i] = Integer.parseInt(arr[i]);
			}
			return result;
		} else {
			throw new NullPointerException("无效的待分割字符串");
		}
	}
	
}
