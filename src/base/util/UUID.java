package base.util;

/**
 * 
 * UUID:
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午10:39:29
 */
public class UUID {
	
	
	public static String getUUID() {
	    String uuid = java.util.UUID.randomUUID().toString().replaceAll("-", "");
	    return uuid;
	}
	  
	public static String[] getLenUUID(int number) {
		
		if (number < 1) {
			return null;
	    }
		String[] retArray = new String[number];
	    for (int i = 0; i < number; i++) {
	    	retArray[i] = getUUID();
	    }
	    return retArray;
	  }
}
