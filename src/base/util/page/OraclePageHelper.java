package base.util.page;


/**
 * 
 * OraclePageHelper
 *
 * @author 姜治昊
 * @time 2018年1月6日 下午6:02:47
 */
public class OraclePageHelper implements PageHelper {

	private int startRow;
	
	private int endRow;
	

	@Override
	public PageHelper getPageParam(int page, int rows) {
		// TODO Auto-generated method stub
		startRow = (page)*rows-rows;
		endRow = page*rows; 
		return this;
	}

	/**
	 * 开始行
	 */
	@Override
	public int getPageParam1() {
		// TODO Auto-generated method stub
		return startRow;
	}

	/**
	 * 结束行
	 */
	@Override
	public int getPageParam2() {
		// TODO Auto-generated method stub
		return endRow;
	}

}
