package base.util.page;


/**
 * 
 * PageHelper
 *
 * @author 姜治昊
 * @time 2018年1月6日 下午6:00:47
 */
public interface PageHelper {

	
	
	/**
	 * 获取具体数据的分页参数
	 * @param page 初始 1
	 * @param rows 初始 分页行数（15）
	 * @return
	 */
	PageHelper getPageParam(int page, int rows);
	
	/**
	 * 获取分页参数1
	 * @return
	 */
	int getPageParam1();
	
	/**
	 * 获取分页参数2
	 * @return
	 */
	int getPageParam2();
	
	
}
