package base.util;

import java.io.Serializable;

/**
 * 
 * ResultMessage:
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午10:39:24
 * @param <T>
 */
public class ResultMessage<T> implements Serializable {

	private static final long serialVersionUID = 6530796271835776987L;

	private Result result;

	private String message;

	private T object;

	public ResultMessage() {
	}

	public ResultMessage(Result result) {
		super();
		this.result = result;
	}

	protected ResultMessage(Result result, T object) {
		super();
		this.result = result;
		this.object = object;
	}

	protected ResultMessage(Result result, String message) {
		super();
		this.result = result;
		this.message = message;
	}

	protected ResultMessage(Result result, String message, T object) {
		super();
		this.result = result;
		this.message = message;
		this.object = object;
	}

	public String getResult() {
		return result.toString();
	}

	public void setResult(Result result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getObject() {
		return object;
	}

	public void setObject(T object) {
		this.object = object;
	}

	@SuppressWarnings("rawtypes")
	public static ResultMessage createSuccessResultMessage() {
		return new ResultMessage(Result.SUCCESS);
	}

	public static <T> ResultMessage<T> createSuccessResultMessage(String message, T object) {
		return new ResultMessage<T>(Result.SUCCESS, message, object);
	}

	public static <T> ResultMessage<T> createSuccessResultMessage(T object) {
		return new ResultMessage<T>(Result.SUCCESS, object);
	}

	@SuppressWarnings("rawtypes")
	public static ResultMessage createSuccessResultMessage(String message) {
		return new ResultMessage(Result.SUCCESS, message);
	}

	@SuppressWarnings("rawtypes")
	public static ResultMessage createErrorResultMessage() {
		return new ResultMessage(Result.ERROR);
	}

	public static <T> ResultMessage<T> createErrorResultMessage(String message, T object) {
		return new ResultMessage<T>(Result.ERROR, message, object);
	}

	public static <T> ResultMessage<T> createErrorResultMessage(T object) {
		return new ResultMessage<T>(Result.ERROR, object);
	}

	@SuppressWarnings("rawtypes")
	public static ResultMessage createErrorResultMessage(String message) {
		return new ResultMessage(Result.ERROR, message);
	}

	enum Result {
		/**
		 * 结果通过字符串验证
		 */
		SUCCESS("SUCCESS"), ERROR("ERROR");
		private String result;

		Result(String result) {
			this.result = result;
		}

		@Override
		public String toString() {
			return result;
		}
	}
}
