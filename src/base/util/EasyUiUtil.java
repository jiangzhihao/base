package base.util;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;

/**
 * 封装DataGrid
 * EasyUIUtil
 *
 * @author 姜治昊
 * @time 2017年12月1日 上午11:53:34
 */
public class EasyUiUtil {

	public static <T> String dataGridFormat(int dataTotal, List<T> data) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("total", dataTotal);
			jsonObject.put("rows",data);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
	
	public static String dataGridFormat(int dataTotal, JSONArray data) {
		JSONObject jsonObject = new JSONObject();
		try {
			jsonObject.put("total", dataTotal);
			jsonObject.put("rows",data);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return jsonObject.toString();
	}
	
}
