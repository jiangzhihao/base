package base.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

/**
 * 
 * PropUtil: 用于读取资源文件
 *
 * @author 姜治昊
 * @time 2017年11月3日 上午10:50:10
 */
public class PropUtil {

	private static Properties props = new Properties();
	static {
		try {
			props.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("config.properties"));
			props.load(Thread.currentThread().getContextClassLoader()
					.getResourceAsStream("jdbc.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static String get(String key) {
		return props.getProperty(key);
	}

	public static void set(String key, String value) {
		props.setProperty(key, value);
	}
	
}
